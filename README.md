##################################################

https://packagist.org/packages/rou9e/framework

##################################################

### Step 01 - Versioning
Updating composer.json to change current version (ex : 1.1.6)

### Step 02 - Committing
gitcommitm "
-- Committing new tag 1.1.6
-- -- Fixing a bug in sitemap generation
-- -- Enhancing and moving getCleanedPostTypesList to \Rou9e\Wordpress\Core
-- -- Updating MetaTags in order to allow the post_types which should display meta fields in backend
"

### Step 03 - Pushing
gitpush

### Step 04 - Tagging
git tag 1.1.6;
git push --tags;

##################################################

### In case of needing to delete a tag
git tag -d 1.1.6;
git push --delete origin 1.1.6;