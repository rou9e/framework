<?php

namespace Rou9e\Wordpress\Pipedrive;

if(!defined('ABSPATH')) { exit; }

class GravityFormsHook {

	public static function run($entry, $form) {
		if(call_user_func(_zCfg['extensions']['Pipedrive']['class'].'::isModuleEnabled')) {
			$formData = \Rou9e\Wordpress\GravityForms::extraDataFromForm($entry, $form);
			if(\Rou9e\Framework\Arr::hasDot($formData, 'form_values.pipedrive_hook')) {
				$r = call_user_func(_zCfg['extensions']['Pipedrive']['class'].'::pushFormDataToPipedrive', $formData['form_values']);
				if(\Rou9e\Wordpress\Pipedrive::DEBUG) { dump($r); exit; }
			}
		}
	}
}