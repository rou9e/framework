<?php

namespace Rou9e\Wordpress\Pipedrive;

// https://developers.elementor.com/docs/form-actions/simple-example/

if(!defined('ABSPATH')) { exit; }

class ElementorHook extends \ElementorPro\Modules\Forms\Classes\Action_Base {

	public function register_settings_section($widget) {}

	public function on_export($element) {}

	public function get_name() {
		return 'Pipedrive by '.ucfirst(_zCfg['guid']);
	}

	public function get_label() {
		return esc_html__($this->get_name(), _zCfg['guid']);
	}

	public function run($record, $ajax_handler) {
		if(call_user_func(_zCfg['extensions']['Pipedrive']['class'].'::isModuleEnabled')) {
			if($ajax_handler->is_success === true) {
				$r = call_user_func(_zCfg['extensions']['Pipedrive']['class'].'::pushFormDataToPipedrive', $record->get('sent_data'));
				if(\Rou9e\Wordpress\Pipedrive::DEBUG) { dump($r); exit; }
			}
		}
	}
}