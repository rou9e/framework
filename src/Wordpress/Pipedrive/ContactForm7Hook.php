<?php

namespace Rou9e\Wordpress\Pipedrive;

if(!defined('ABSPATH')) { exit; }

class ContactForm7Hook {

	public static function run($form) {
		if(call_user_func(_zCfg['extensions']['Pipedrive']['class'].'::isModuleEnabled')) {
			$submission = \WPCF7_Submission::get_instance();
			if($submission) {
				$formData = $submission->get_posted_data();
				if(!is_array($formData)) $formData = [];
				if(\Rou9e\Framework\Arr::hasDot($formData, 'pipedrive_hook')) {
					$r = call_user_func(_zCfg['extensions']['Pipedrive']['class'].'::pushFormDataToPipedrive', $formData);
					if(\Rou9e\Wordpress\Pipedrive::DEBUG) { dump($r); exit; }
				}
			}
		}
	}
}
