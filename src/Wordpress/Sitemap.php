<?php

namespace Rou9e\Wordpress;

// \Rou9e\Wordpress\Sitemap::isModuleEnabled();

class Sitemap extends \Rou9e\Wordpress\AbstractHelper {

	// #################################################################################################### initExtension
	public static function initExtension() {
		parent::initExtension();
		if(\Rou9e\Wordpress\Sitemap::isModuleEnabled() === true) {
			add_filter('wp_sitemaps_enabled', '__return_false', PHP_INT_MAX, 1);
		}
	}

	// #################################################################################################### initAdminSettingsMenu
	public static function initAdminSettingsMenu() {
		static::adminAddSettingsMenu([
			'parent'									=> static::getGuid().'_container',
			'code'										=> static::getClassGuid(),
			'label'										=> static::getClassShortName(),
		]);
	}

	// #################################################################################################### initAdminSettingsPage
	public static function initAdminSettingsPage() {
		static::renderAdminSettingsPage([
			'title'										=> ucfirst(static::getGuid()).' '.static::getClassShortName(),
			'description'							=> "Here you can setup some additional configuration. Remember to setup the configuration for every language if you are using WPML.",
			'acf'											=> true,
		]);
	}

	// #################################################################################################### initAcfGroupAndFields
	public static function initAcfGroupAndFields() {
		static::createAfcForm([
			'fields'									=> [
				[
					'type'								=> "true_false",
					'code'								=> 'enabled',
					'label'								=> "Enabled",
					'instructions'				=> "Enable / Disable the sitemap generation",
					'default_value'				=> 1,
				],
				[
					'type'								=> "true_false",
					'code'								=> 'include_lastmod',
					'label'								=> "Include lastmod",
					'instructions'				=> "Include the date of the last modification for each url",
					'default_value'				=> 1,
				],
				[
					'type'								=> "true_false",
					'code'								=> 'include_homepage',
					'label'								=> "Include homepage",
					'instructions'				=> "Include the homepage in the sitemap",
					'default_value'				=> 1,
				],
				[
					'type'								=> "true_false",
					'code'								=> 'include_categories',
					'label'								=> "Include categories",
					'instructions'				=> "Include the categories in the sitemap",
					'default_value'				=> 1,
				],
				[
					'type'								=> "true_false",
					'code'								=> 'exclude_empty_categories',
					'label'								=> "Exclude empty categories",
					'instructions'				=> "Exclude the empty categories in the sitemap",
					'default_value'				=> 0,
				],
				[
					'type'								=> "true_false",
					'code'								=> 'include_tags',
					'label'								=> "Include tags",
					'instructions'				=> "Include the tags in the sitemap",
					'default_value'				=> 1,
				],
				[
					'type'								=> "true_false",
					'code'								=> 'exclude_empty_tags',
					'label'								=> "Exclude empty tags",
					'instructions'				=> "Exclude the empty tags in the sitemap",
					'default_value'				=> 0,
				],
				[
					'type'								=> "select",
					'code'								=> 'include_post_types',
					'label'								=> "Include post_types",
					'instructions'				=> "Select the post_types to include in the sitemap",
					'options'							=> [
						'choices'						=> \Rou9e\Wordpress\Core::getCleanedPostTypesList(),
						'default_value'			=> ['post', 'page'],
						'allow_null'				=> 0,
						'multiple'					=> 1,
						'return_format'			=> 'value',
						'ajax'							=> 0,
					],
				],
				[
					'type'								=> "textarea",
					'code'								=> 'excluded_posts',
					'label'								=> "Excluded posts / pages",
					'instructions'				=> "List the IDs of posts / pages to exclude (comma separated)",
					'options'							=> ['rows' => 4],
				],
				[
					'type'								=> "select",
					'code'								=> 'excluded_categories',
					'label'								=> "Excluded categories",
					'instructions'				=> "Select the categories to exclude from the sitemap",
					'options'							=> [
						'conditional_logic'	=> [[['field' => static::getClassGuid().'-include_categories', 'operator' => '==', 'value' => '1']]],
						'choices'					=> static::getPostsCategoriesChoices(),
						'default_value'		=> ['post', 'page'],
						'allow_null'			=> 0,
						'multiple'				=> 1,
						'return_format'		=> 'value',
						'ajax'						=> 0,
					],
				],
				[
					'type'								=> "select",
					'code'								=> 'excluded_tags',
					'label'								=> "Excluded tags",
					'instructions'				=> "Select the tags to exclude from the sitemap",
					'options'							=> [
						'conditional_logic'	=> [[['field' => static::getClassGuid().'-include_tags', 'operator' => '==', 'value' => '1']]],
						'choices'					=> static::getPostsTagsChoices(),
						'default_value'		=> ['post', 'page'],
						'allow_null'			=> 0,
						'multiple'				=> 1,
						'return_format'		=> 'value',
						'ajax'						=> 0,
					],
				],
				[
					'type'								=> "textarea",
					'code'								=> 'sitemap_configuration',
					'label'								=> "Sitemap configuration",
					'instructions'				=> "JSON allowing to configure priority and changefreq for each of elements (homepage, category, default, page, post, etc)",
					'options'							=> ['rows' => 16],
				],
				[
					'type'								=> "message",
					'code'								=> 'sitemap_configuration_explanations',
					'label'								=> "Sitemap configuration (explanations)",
					'instructions'				=> "Here an example of JSON configuration",
					'options'							=> [
						'rows'							=> 4,
						'message'						=> '
<pre style="margin: 0;padding: 10px;border: 1px solid #e0e0e0;border-radius: 4px;background-color: #f9f9f9;">
{
  "homepage":{
    "priority":"1.0",
    "changefreq":"daily"
  },
  "category":{
    "priority":"0.6",
    "changefreq":"daily"
  },
  "tag":{
    "priority":"0.6",
    "changefreq":"daily"
  },
  "default":{
    "priority":"0.5",
    "changefreq":"weekly"
  },
  "page":{
    "priority":"0.8",
    "changefreq":"daily"
  },
  "post":{
    "priority":"0.8",
    "changefreq":"daily"
  },
  "some_post_type":{
    "priority":"0.8",
    "changefreq":"daily"
  }
}
	</pre>
<p>
// identifier : homepage, category, tag, default, each of included post_type name<br/>
// priority : 0.0 to 1.0<br/>
// changefreq : always | hourly | daily | weekly | monthly | yearly | never
</p>',
					],
				],
				[
					'type'								=> "message",
					'code'								=> 'sitemap_url',
					'label'								=> "Sitemap url",
					'instructions'				=> "If the link does not work, you should try to save the permalink settings again",
					'options'							=> [
						'rows'							=> 4,
						'message'						=> '<a href="'.\Rou9e\Wordpress\Core::getBaseUrl().'sitemap.xml'.'" target="_blank">'.\Rou9e\Wordpress\Core::getBaseUrl().'sitemap.xml'.'</a>',
					],
				],
			],
		]);
	}

	// #################################################################################################### initFrontendPages
	public static function initFrontendPages() {
		// guid_sitemap
		static::renderFrontPage([
			'url_key'									=> static::getGuid(),
			'code'										=> 'display_sitemap',
			'callback'								=> '\Rou9e\Wordpress\Sitemap::renderSitemapXml',
		]);
		// sitemap.xml
		static::renderFrontPage([
			'url_key'									=> 'sitemap.xml',
			'code'										=> 'display_sitemap',
			'callback'								=> '\Rou9e\Wordpress\Sitemap::renderSitemapXml',
		]);
	}

	// #################################################################################################### renderSitemapXml
	public static function renderSitemapXml() {
		if(\Rou9e\Wordpress\Sitemap::isModuleEnabled() === true) {
			$items = [];
			// ######################### Handling homepage
			$posts = []; $url = ''; $hash = '';
			if(\Rou9e\Wordpress\Sitemap::includeHomepage() === true) {
				$url = \Rou9e\Wordpress\Core::getHomeUrl();
				$hash = md5($url);
				if(!isset($items[$hash])) $items[$hash] = ['type' => 'homepage', 'url' => $url, 'date' => date('Y-m-d')];
			}
			// ######################### Handling categories
			$posts = []; $url = ''; $hash = '';
			if(\Rou9e\Wordpress\Sitemap::includeCategories() === true) {
				$categories = \Rou9e\Wordpress\Core::getPostsCategories();
				foreach($categories as $category) {
					if(!in_array($category->cat_ID, \Rou9e\Wordpress\Sitemap::getExcludedCategories())) {
						$url = get_category_link($category->cat_ID);
						$hash = md5($url);
						if(!isset($items[$hash])) {
							$query = new \WP_Query([
								'post_type'						=> 'post',
								'post_status'					=> 'publish',
								'posts_per_page'			=> 1,
								'orderby'							=> 'modified',
								'order'								=> 'DESC',
								'cat_id'							=> $category->cat_ID,
							]);
							$posts = $query->posts;
							wp_reset_postdata();
							if(!empty($posts) && isset($posts[0])) {
								$items[$hash] = ['type' => 'category', 'url' => $url, 'date' => strtok($posts[0]->post_modified, ' ')];
							}
							else {
								if(\Rou9e\Wordpress\Sitemap::excludeEmptyCategories() === false) {
									$items[$hash] = ['type' => 'category', 'url' => $url];
								}
							}
						}
					}
				}
			}
			// ######################### Handling tags
			$posts = []; $url = ''; $hash = '';
			if(\Rou9e\Wordpress\Sitemap::includeTags() === true) {
				$tags = \Rou9e\Wordpress\Core::getPostsTags();
				foreach($tags as $tag) {
					if(!in_array($tag->term_id, \Rou9e\Wordpress\Sitemap::getExcludedTags())) {
						$url = get_tag_link($tag->term_id);
						$hash = md5($url);
						if(!isset($items[$hash])) {
							$query = new \WP_Query([
								'post_type'						=> 'post',
								'post_status'					=> 'publish',
								'posts_per_page'			=> 1,
								'orderby'							=> 'modified',
								'order'								=> 'DESC',
								'tag_id'							=> $tag->term_id,
							]);
							$posts = $query->posts;
							wp_reset_postdata();
							if(!empty($posts) && isset($posts[0])) {
								$items[$hash] = ['type' => 'tag', 'url' => $url, 'date' => strtok($posts[0]->post_modified, ' ')];
							}
							else {
								if(\Rou9e\Wordpress\Sitemap::excludeEmptyTags() === false) {
									$items[$hash] = ['type' => 'tag', 'url' => $url];
								}
							}
						}
					}
				}
			}
			// ######################### Handling post_types
			$posts = []; $url = ''; $hash = '';
			if(!empty(\Rou9e\Wordpress\Sitemap::getIncludedPostsTypes())) {
				$query = new \WP_Query([
					'post_type'						=> \Rou9e\Wordpress\Sitemap::getIncludedPostsTypes(),
					'post_status'					=> 'publish',
					'posts_per_page'			=> -1,
					'orderby'							=> 'modified',
					'order'								=> 'DESC',
					'post__not_in'				=> \Rou9e\Wordpress\Sitemap::getExcludedPosts(),
				]);
				$posts = $query->posts;
				wp_reset_postdata();
				foreach($posts as $post) {
					$url = get_permalink($post);
					$hash = md5($url);
					if(!isset($items[$hash])) $items[$hash] = ['type' => $post->post_type, 'url' => $url, 'date' => strtok($post->post_modified, ' ')];
				}
			}
			// ######################### Double checking homepage (maybe be inside 'post_types')
			$posts = []; $url = ''; $hash = '';
			if(\Rou9e\Wordpress\Sitemap::includeHomepage() === false) {
				$url = \Rou9e\Wordpress\Core::getHomeUrl();
				$hash = md5($url);
				if(isset($items[$hash])) unset($items[$hash]);
			}
			// ######################### Handling XML
			$config = \Rou9e\Wordpress\Sitemap::getSitemapConfiguration();
			$xml = '<?xml version="1.0" encoding="UTF-8"?>';
			$xml .= "\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
			foreach($items as $item) {
				$xml .= "\n\t".'<url>';
				$xml .= "\n\t\t".'<loc>'.$item['url'].'</loc>';
				if(\Rou9e\Wordpress\Sitemap::includeLastmod() && isset($item['date'])) {
					$xml .= "\n\t\t".'<lastmod>'.$item['date'].'</lastmod>';
				}
				$changefreq = (isset($config[$item['type']])) ? $config[$item['type']]['changefreq']: $config['default']['changefreq'];
				$priority = (isset($config[$item['type']])) ? $config[$item['type']]['priority']: $config['default']['priority'];
				$xml .= "\n\t\t".'<changefreq>'.$changefreq.'</changefreq>';
				$xml .= "\n\t\t".'<priority>'.$priority.'</priority>';
				$xml .= "\n\t".'</url>';
			}
			$xml .= "\n".'</urlset>';
			// ######################### Outputting
			header('HTTP/1.1 200 OK');
			header('Content-Type: application/xml; charset=UTF-8');
			echo $xml;
		}
		else {
			global $wp_query;
			$wp_query->set_404();
			status_header(404);
			get_template_part(404);
		}
	}

	// #################################################################################################### getPostsCategoriesChoices
	public static function getPostsCategoriesChoices(): array {
		$categories = [];
		foreach(\Rou9e\Wordpress\Core::getPostsCategories() as $category) {
			$categories[$category->cat_ID] = $category->name.' ('.$category->cat_ID.')';
		}
		return $categories;
	}

	// #################################################################################################### getPostsTagsChoices
	public static function getPostsTagsChoices(): array {
		$tags = [];
		foreach(\Rou9e\Wordpress\Core::getPostsTags() as $tag) {
			$tags[$tag->term_id] = $tag->name.' ('.$tag->term_id.')';
		}
		return $tags;
	}

	// #################################################################################################### includeLastmod
	public static function includeLastmod() {
		return \Rou9e\Wordpress\Acf::getAcfOptionBooleanValue(static::getClassGuid().'-include_lastmod');
	}

	// #################################################################################################### includeHomepage
	public static function includeHomepage() {
		return \Rou9e\Wordpress\Acf::getAcfOptionBooleanValue(static::getClassGuid().'-include_homepage');
	}

	// #################################################################################################### includeCategories
	public static function includeCategories() {
		return \Rou9e\Wordpress\Acf::getAcfOptionBooleanValue(static::getClassGuid().'-include_categories');
	}

	// #################################################################################################### excludeEmptyCategories
	public static function excludeEmptyCategories() {
		return \Rou9e\Wordpress\Acf::getAcfOptionBooleanValue(static::getClassGuid().'-exclude_empty_categories');
	}

	// #################################################################################################### includeTags
	public static function includeTags() {
		return \Rou9e\Wordpress\Acf::getAcfOptionBooleanValue(static::getClassGuid().'-include_tags');
	}

	// #################################################################################################### excludeEmptyTags
	public static function excludeEmptyTags() {
		return \Rou9e\Wordpress\Acf::getAcfOptionBooleanValue(static::getClassGuid().'-exclude_empty_tags');
	}

	// #################################################################################################### getExcludedPosts
	public static function getExcludedPosts() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-excluded_posts');
		$value = str_replace(' ', '', $value);
		return \Rou9e\Framework\Core::cleanList($value);
	}

	// #################################################################################################### getIncludedPostsTypes
	public static function getIncludedPostsTypes() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-include_post_types');
		if($value === '') $value = [];
		return $value;
	}

	// #################################################################################################### getExcludedCategories
	public static function getExcludedCategories() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-excluded_categories');
		if($value === '') $value = [];
		return $value;
	}

	// #################################################################################################### getExcludedTags
	public static function getExcludedTags() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-excluded_tags');
		if($value === '') $value = [];
		return $value;
	}

	// #################################################################################################### getSitemapConfiguration
	public static function getSitemapConfiguration() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionJsonValue(static::getClassGuid().'-sitemap_configuration');
		// Checking JSON is an array
		if(!is_array($value)) \Rou9e\Framework\Messages::throwException('NOT_ARRAY', ['what' => "Sitemap JSON configuration"]);
		$configuration = [];
		// Looping on items
		foreach($value as $k => $item) {
			if(is_array($item) && isset($item['changefreq']) && isset($item['priority'])) {
				if(in_array($item['changefreq'], ['always', 'hourly', 'daily', 'weekly', 'monthly', 'yearly', 'never'])) {
					if(in_array($item['priority'], ['0', '0.0', '0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9', '1.0', '1'])) {
						// Keeping the item if it is well constituted (valid changefreq & priority)
						$configuration[$k] = $item;
 					}
 				}
			}
		}
		// Adding a default value to fallback on if no one is specified
		if(!isset($configuration['default']) || !isset($configuration['default']['changefreq']) || !isset($configuration['default']['priority'])) {
			$configuration['default'] = ['priority' => '0.5', 'changefreq' => 'weekly'];
		}
		return $configuration;
	}
}
