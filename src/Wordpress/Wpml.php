<?php

namespace Rou9e\Wordpress;

// \Rou9e\Wordpress\Wpml::isModuleEnabled();

class Wpml extends \Rou9e\Wordpress\AbstractHelper {

	// #################################################################################################### initExtension
	public static function initExtension() {
		// Calling parent function
		parent::initExtension();
		// Adding CSS / JS assets to the admin
		wp_enqueue_script(static::getGuid().'-elementor-dialog-js', \Rou9e\Wordpress\Core::getPluginsUrl().'elementor/assets/lib/dialog/dialog.min.js', ['jquery'], '', true);
	}

	// #################################################################################################### addShortcodes
	public static function initShortcodes(array $options = []) {
		if(static::isModuleEnabled() === true) {
			add_shortcode('get_custom_wpml_popin_cta', function() {
				echo \Rou9e\Wordpress\Wpml::getCustomWpmlPopinCta('');
			});
			add_shortcode('get_custom_wpml_popin', function() {
				echo \Rou9e\Wordpress\Wpml::getCustomWpmlPopin('');
			});
			add_shortcode('get_custom_wpml_selector', function() {
				echo \Rou9e\Wordpress\Wpml::getCustomWpmlSelector('');
			});
		}
	}

	// #################################################################################################### initAdminSettingsMenu
	public static function initAdminSettingsMenu() {
		static::adminAddSettingsMenu([
			'parent'									=> static::getGuid().'_container',
			'code'										=> static::getClassGuid(),
			'label'										=> static::getClassShortName(),
		]);
	}

	// #################################################################################################### initAdminSettingsPage
	public static function initAdminSettingsPage() {
		static::renderAdminSettingsPage([
			'title'										=> ucfirst(static::getGuid()).' '.static::getClassShortName(),
			'description'							=> "Here you can setup some additional configuration. Remember to setup the configuration for every language if you are using WPML.",
			'acf'											=> true,
		]);
	}

	// #################################################################################################### initAcfGroupAndFields
	public static function initAcfGroupAndFields() {
		static::createAfcForm([
			'fields'									=> [
				[
					'type'								=> "true_false",
					'code'								=> 'enabled',
					'label'								=> "Enabled",
					'instructions'				=> "Enable / Disable the WPML extension",
					'default_value'				=> 0,
				],
				[
					'type'								=> "textarea",
					'code'								=> 'selector_configuration',
					'label'								=> "Selector configuration",
					'instructions'				=> "JSON allowing to configure how the languages / countries selector appears",
					'options'							=> ['rows' => 22],
					'default_value'				=> '
{
	"2columns": false,
	"groups": [
		{
			"code": "world",
			"label": "World",
			"icon": "wo",
			"children": [
				{
					"code": "fr",
					"label": "FR",
					"icon": "fr"
				},
				{
					"code": "en",
					"label": "EN",
					"icon": "us"
				}
			]
		}
	]
}',
				],
				[
					'type'								=> "message",
					'code'								=> 'selector_configuration_explanations',
					'label'								=> "Selector configuration (explanations)",
					'instructions'				=> "Here an example of JSON configuration",
					'options'							=> [
						'message'						=> '
<pre style="margin: 0;padding: 10px;border: 1px solid #e0e0e0;border-radius: 4px;background-color: #f9f9f9;">
{
  "2columns": false,
  "groups": [
    {
      "code": "world",
      "label": "World",
      "icon": "wo",
      "children": [
        {
          "code": "fr",
          "label": "FR",
          "icon": "fr"
        },
        {
          "code": "en",
          "label": "EN",
          "icon": "us"
        }
      ]
    }
  ]
}
</pre>
<pre style="margin: 20px 0 0;padding: 10px;border: 1px solid #e0e0e0;border-radius: 4px;background-color: #f9f9f9;">
{
  "2columns": true,
  "groups": [
    {
      "code": "french",
      "label": "France",
      "icon": "fr",
      "children": [
        {
          "code": "fr",
          "label": "Français",
          "icon": "fr"
        }
      ]
    },
    {
      "code": "english",
      "label": "United Kingdom",
      "icon": "uk",
      "children": [
        {
          "code": "en",
          "label": "English",
          "icon": "uk"
        }
      ]
    },
    {
      "code": "belgium",
      "label": "Belgique",
      "icon": "be",
      "children": [
        {
          "code": "be",
          "label": "Français",
          "icon": "be"
        }
      ]
    },
    {
      "code": "switzerland",
      "label": "Suisse",
      "icon": "ch",
      "children": [
        {
          "code": "ch",
          "label": "Français",
          "icon": "ch"
        }
      ]
    },
    {
      "code": "luxembourg",
      "label": "Luxembourg",
      "icon": "lu",
      "children": [
        {
          "code": "lu",
          "label": "Français",
          "icon": "lu"
        }
      ]
    },
    {
      "code": "spain",
      "label": "España",
      "icon": "es",
      "children": [
        {
          "code": "es",
          "label": "Español",
          "icon": "es"
        }
      ]
    },
    {
      "code": "italian",
      "label": "Italia",
      "icon": "it",
      "children": [
        {
          "code": "it",
          "label": "Italiano",
          "icon": "it"
        }
      ]
    },
    {
      "code": "german",
      "label": "Deutschland",
      "icon": "de",
      "children": [
        {
          "code": "de",
          "label": "Deutsche",
          "icon": "de"
        }
      ]
    }
  ]
}
</pre>',
					],
				],
			],
		]);
	}

	// #################################################################################################### isWpmlInstalled
	public static function isWpmlInstalled(): bool {
		return (class_exists('SitePress'));
	}

	// #################################################################################################### Getting every active languages
	public static function getActiveLanguages() {
		return apply_filters('wpml_active_languages', null);
	}

	// #################################################################################################### Getting a specific language by code
	public static function getLanguage(string $code = '') {
		if($code == '') return false;
		$languages = static::getActiveLanguages();
		if(isset($languages[$code])) return $languages[$code];
		return false;
	}

	// #################################################################################################### Getting current language
	public static function getCurrentLanguage() {
		foreach(static::getActiveLanguages() as $language) {
			if($language['active'] === '1') return $language;
		}
		return false;
	}

	// #################################################################################################### Getting current language code
	public static function getCurrentLanguageCode() {
		if(static::getCurrentLanguage() === false) return false;
		return static::getCurrentLanguage()['code'];
	}

	// #################################################################################################### Getting default language
	public static function getDefaultLanguage() {
		return apply_filters('wpml_default_language', null);
	}

	// #################################################################################################### Getting the current language translation
	public static function getCurrentLanguageTranslation(string $code = '') {
		if($code == '') return false;
		$languages = static::getActiveLanguages();
		if(!isset($languages[$code])) return false;
		return $languages[$code];
	}

	#################################################################################################### WpmlPopin : Allowing to render a nice flag / icon for a language
	public static function getCustomFlagIcon(string $icon = '') {
		if($icon == '') return false;
		$file = 'pub/img/flags/'.$icon.'.png';
		$filepath = \Rou9e\Wordpress\Core::getFrameworkPath().$file;
		if(!file_exists($filepath)) {
			\Rou9e\Wordpress\Core::log($filepath.' not found');
			return '';
		}
		return \Rou9e\Wordpress\Core::getFrameworkUrl().$file;
	}

	#################################################################################################### WpmlPopin : Allowing to render the CTA of the custom WPML popin
	public static function getCustomWpmlPopinCta(string $cssClass = '') {
		$lang = static::getCurrentLanguage();
		if(!$lang) return '';
		$cssClass = ($cssClass == '') ? static::getGuid().'-wpml-cta' : static::getGuid().'-wpml-cta '.$cssClass;
		$html = '';
		$html .= '<div class="'.$cssClass.'" data-code="'.$lang['code'].'">';
		$html .= 	'<img class="img-fluid" src="'.static::getIconFromCode($lang['code']).'" alt="'.__("Select another language", static::getGuid()).'" title="'.__("Select another language", static::getGuid()).'" />';
		$html .= '</div>';
		$html .= '<style>';
		$html .= 	'.'.static::getGuid().'-wpml-cta{display: inline-block;width: 25px;margin: 5px;cursor: pointer;}';
		$html .= 	'.'.static::getGuid().'-wpml-cta img{}';
		$html .= 	'.'.static::getGuid().'-wpml-selector{min-width: 300px;text-align: center;padding: 20px 20px 5px;}';
		$html .= 	'.'.static::getGuid().'-wpml-selector-row{}';
		$html .= 	'.'.static::getGuid().'-wpml-selector-row:not(:last-child){margin-bottom: 20px;padding-bottom: 10px;border-bottom: 1px solid #e5e5e5;}';
		$html .= 	'.'.static::getGuid().'-wpml-selector-row-2columns{width:50%;float:left;margin: 20px auto 10px;}';
		$html .= 	'.'.static::getGuid().'-wpml-selector-row-img{width: 50px;margin-bottom: 10px;}';
		$html .= 	'.'.static::getGuid().'-wpml-selector-row-label{text-transform: uppercase;margin-bottom: 10px;}';
		$html .= 	'.'.static::getGuid().'-wpml-selector-row-langs{margin-bottom: 10px;}';
		$html .= 	'.'.static::getGuid().'-wpml-selector-row-langs-item{padding: 5px;}';
		$html .= 	'.'.static::getGuid().'-wpml-popin-ctn{}';
		$html .= 	'.'.static::getGuid().'-wpml-popin{display:none;}';
		$html .= 	'#'.static::getGuid().'-wpml-popin-ctn{z-index: 1000;background-color: rgba(0,0,0,0.75);}';
		$html .= 	'#'.static::getGuid().'-wpml-popin-ctn .'.static::getGuid().'-wpml-popin{display:block;}';
		$html .= 	'#'.static::getGuid().'-wpml-popin-ctn .dialog-header{display:none;}';
		$html .= 	'#'.static::getGuid().'-wpml-popin-ctn .dialog-widget-content{max-height: 85%;margin-top: 50px;left: 50%;margin-left: -175px;overflow-y: auto;height: auto;}';
		$html .= 	'#'.static::getGuid().'-wpml-popin-ctn .dialog-buttons-wrapper{display:none;}';
		$html .= 	'#'.static::getGuid().'-wpml-popin-ctn .dialog-message{padding:0;font-size: unset;line-height: unset;}';
		$html .= 	'.'.static::getGuid().'-wpml-cta-footer{margin: -25px auto 15px;display: block;}';
		$html .= '</style>';
		$html .= '<script type="text/javascript">';
		$html .= 		'jQuery(document).on("click", ".'.static::getGuid().'-wpml-cta", function(event) {';
		$html .= 			'event.stopPropagation();';
		$html .= 			'jQuery("#'.static::getGuid().'-wpml-popin-ctn").remove();';
		$html .= 			'var dialogManager = new DialogsManager.Instance();';
		$html .= 			'var confirmWidget = dialogManager.createWidget("alert");';
		$html .= 			'var popinContent = jQuery(".'.static::getGuid().'-wpml-popin").first().clone();';
		$html .= 			'confirmWidget.setID("'.static::getGuid().'-wpml-popin-ctn");';
		$html .= 			'confirmWidget.setMessage(popinContent);';
		$html .= 			'confirmWidget.show();';
		$html .= 		'});';
		$html .= '</script>';
		return $html;
	}

	#################################################################################################### WpmlPopin : Allowing to render the HTML of the custom WPML popin
	public static function getCustomWpmlPopin(string $cssClass = '') {
		$cssClass = ($cssClass == '') ? static::getGuid().'-wpml-popin' : static::getGuid().'-wpml-popin '.$cssClass;
		$html = '';
		$html .= '<div class="'.$cssClass.'">';
		$html .= 	static::getCustomWpmlSelector();
		$html .= '</div>';
		return $html;
	}

	#################################################################################################### WpmlPopin : Allowing to render the HTML of the custom WPML selector
	public static function getCustomWpmlSelector(string $cssClass = '') {
		try {
			$cssClass = ($cssClass == '') ? static::getGuid().'-wpml-selector' : static::getGuid().'-wpml-selector '.$cssClass;
			$options = \Rou9e\Wordpress\Acf::getAcfOptionJsonValue(static::getGuid().'_wpml-selector_configuration');
		}
		catch(\Exception $e) {
			\Rou9e\Wordpress\Core::log($e->getMessage());
			return '';
		}
		$html = '';
		$html .= '<div class="'.static::getGuid().'-wpml-selector">';
		$cssClassRow = ($options['2columns']) ? static::getGuid().'-wpml-selector-row-2columns' : '';
		foreach($options['groups'] as $group) {
			$html .= 	'<div class="'.static::getGuid().'-wpml-selector-row '.static::getGuid().'-wpml-selector-row-'.$group['code'].' '.$cssClassRow.'">';
			$html .= 		'<img class="'.static::getGuid().'-wpml-selector-row-img" src="'.static::getCustomFlagIcon($group['icon']).'" alt="'.__($group['label'], static::getGuid()).'" title="'.__($group['label'], static::getGuid()).'">';
			$html .= 		'<div class="'.static::getGuid().'-wpml-selector-row-label">'.__($group['label'], static::getGuid()).'</div>';
			$html .= 		'<div class="'.static::getGuid().'-wpml-selector-row-langs">';
			foreach($group['children'] as $child) {
				$translation = static::getCurrentLanguageTranslation($child['code']);
				if($translation !== false) {
					$html .= 			'<a class="'.static::getGuid().'-wpml-selector-row-langs-item '.static::getGuid().'-wpml-selector-row-langs-item-'.$child['code'].'" href="'.$translation['url'].'" title="'.__($child['label'], static::getGuid()).'">'.__($child['label'], static::getGuid()).'</a>';
				}
			}
			$html .= 		'</div>';
			$html .= 	'</div>';
		}
		$html .= '</div>';
		return $html;
	}

	#################################################################################################### WpmlPopin : Allowing to get theme WPML popin options
	public static function getCustomWpmlPopinVerifiedOptions() {
		$options = \Rou9e\Wordpress\Acf::getAcfOptionJsonValue(static::getGuid().'_wpml-selector_configuration');
		if(!isset($options['2columns']) || !is_bool($options['2columns'])) $options['2columns'] = false;
		if(!isset($options['groups']) || !is_array($options['groups'])) $options['groups'] = [];
		if(empty($options['groups'])) \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => '$options["groups"]']);
		foreach($options['groups'] as $group) {
			if(!isset($group['code']) || !is_string($group['code']) || ($group['code'] === '')) \Rou9e\Framework\Messages::throwException('NOT_VALID', ['what' => '$options["groups"][?]["code"]']);
			if(!isset($group['label']) || !is_string($group['label']) || ($group['label'] === '')) \Rou9e\Framework\Messages::throwException('NOT_VALID', ['what' => '$options["groups"][?]["label"]']);
			if(!isset($group['icon']) || !is_string($group['icon']) || ($group['icon'] === '')) \Rou9e\Framework\Messages::throwException('NOT_VALID', ['what' => '$options["groups"][?]["icon"]']);
			if(!isset($group['children']) || !is_array($group['children']) || (empty($group['children']))) \Rou9e\Framework\Messages::throwException('NOT_VALID', ['what' => '$options["groups"][?]["children"]']);
			foreach($group['children'] as $child) {
				if(!isset($child['code']) || !is_string($child['code']) || ($child['code'] === '')) \Rou9e\Framework\Messages::throwException('NOT_VALID', ['what' => '$options["groups"][?]["children"][?]["code"]']);
				if(!isset($child['label']) || !is_string($child['label']) || ($child['label'] === '')) \Rou9e\Framework\Messages::throwException('NOT_VALID', ['what' => '$options["groups"][?]["children"][?]["label"]']);
				if(!isset($child['icon']) || !is_string($child['icon']) || ($child['icon'] === '')) $child['icon'] = $child['code'];
			}
		}
		if(count($options['groups']) == 1) $options['2columns'] = false;
		return $options;
	}

	#################################################################################################### WpmlPopin : Allowing to get the icon from code
	public static function getIconFromCode(string $code = '') {
		$options = static::getCustomWpmlPopinVerifiedOptions();
		$icons = [];
		foreach($options['groups'] as $group) {
			foreach($group['children'] as $child) {
				$icons[$child['code']] = $child['icon'];
			}
		}
		if(!isset($icons[$code]) || empty($code)) return '';
		return static::getCustomFlagIcon($icons[$code]);
	}
}
