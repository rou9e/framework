<?php

namespace Rou9e\Wordpress;

/*
	Differents ways to call callback functions
	Best way
		>>> [static::getClassName(), 'initAdminSettingsPage'],
	Others ways
		>>> 'myFunctionName',
		>>> '\Rou9e\Wordpress\Core::initAdminSettingsPage',
		>>> [static::getClassName(), $options['callback']],
		>>> function() { return static::overrideSearchUrl(); });
*/

abstract class AbstractHelper {

	// #################################################################################################### init
	public static function init() {
		static::initExtension();
		static::initFrontendPages();
		static::initAdminSettingsMenu();
		static::initAcfGroupAndFields();
		static::initShortcodes();
		add_action('admin_head', [static::getClassName(), 'initInjectHtmlInAdminHead']);
		add_action('admin_footer', [static::getClassName(), 'initInjectHtmlInAdminFooter']);
}

	// #################################################################################################### Empty init functions
	public static function initFrontendPages() {}								// Used to handle frontend pages
	public static function initAdminSettingsMenu() {}						// Used to add menu item in the admin
	public static function initAcfGroupAndFields() {}						// Used to create a ACF group / fields
	public static function initInjectHtmlInAdminHead() {}				// Used to add HTML code in the head in the admin
	public static function initInjectHtmlInAdminFooter() {}			// Used to add HTML code in the footer in the admin
	public static function initShortcodes() {}									// Used to define / register shortcodes
	public static function initHooks() {}												// Used to initiate hooks

	// #################################################################################################### isModuleEnabled
	public static function isModuleEnabled(): bool {
		if(in_array(static::getClassShortName(), ['Core', 'Acf'])) return true;
		$value = \Rou9e\Wordpress\Acf::getAcfOptionBooleanValue(static::getClassGuid().'-enabled', false);
		if(is_bool($value)) return $value;
		return false;
	}

	// #################################################################################################### isModuleEnabled
	public static function initExtension() {
		// Adding CSS classes to body
		add_action('admin_init', function() {
			$urlData = \Rou9e\Framework\Core::parseUrl('', true);
			$urlPath = @$urlData['path'] ?? '';
			$urlPage = '';
			if(\Rou9e\Framework\Arr::hasDot($urlData, 'get.page')) $urlPage = @$urlData['get']['page'] ?? '';
			if(($urlPath === 'wp-admin/admin.php') && ($urlPage === static::getClassGuid())) {
				static::adminAddCssClassToBody('zFramework-settings-page');
				static::adminAddCssClassToBody('zFramework-settings-page-'.static::getClassGuid());
			}
			// Handling hidden errors
			$error = error_get_last();
			if($error !== null) {
				$html = '<div class="error-php">';
				$html .= '<h3 class="error-php-title">PHP Error #'.$error['type'].' : '.$error['message'].'</h3>';
				$html .= '<pre class="error-php-content"><xmp>'.$error['file'].' ('.$error['line'].')</xmp></pre>';
				$html .= '</div>';
				echo $html;
			}
		});
	}

	// #################################################################################################### initAdminSettingsPage
	public static function initAdminSettingsPage() {
		static::renderAdminSettingsPage([
			'title'			=> __("Thanks for using "._zCfg['guid']." framework !", _zCfg['guid']),
			'baseline'	=> __('Take a look to the submenus on the left sidebar. You can also say hi on our website : <a href="'._zCfg['website'].'" target="_blank">'._zCfg['website'].'</a>', _zCfg['guid']),
		]);
	}

	// #################################################################################################### getClassName
	public static function getClassName(): string {
		return '\\'.static::class;
	}

	// #################################################################################################### getClassShortName
	public static function getClassShortName(): string {
		$className = static::getClassName();
		$tmp = explode('\\', $className);
		$classShortName = array_pop($tmp);
		return $classShortName;
	}

	// #################################################################################################### getClassGuid
	public static function getGuid(): string {
		return _zCfg['guid'];
	}

	// #################################################################################################### getClassGuid
	public static function getClassGuid(): string {
		return static::getGuid().'_'.strtolower(static::getClassShortName());
	}

	// #################################################################################################### adminShowNotification
	public static function adminShowNotification(string $message = '', string $type = '') {
		if(!in_array($type, ['', 'error', 'warning', 'success', 'info'])) $type = '';
		add_action('admin_notices', function() use ($message, $type) {
			$class = ($type === '') ? '' : 'notice-'.$type;
			echo '<div class="notice '.$class.'"><p>'.__($message, _zCfg['guid']).'</p></div>';
		});
	}

	// #################################################################################################### adminAddCssClassToBody
	public static function adminAddCssClassToBody(string $class = '') {
		$class = trim($class);
		if($class !== '') {
			add_filter('admin_body_class', function($classes) use ($class) {
				$classes .= ' '.$class;
		    return $classes;
			});
		}
	}

	// #################################################################################################### adminAddSettingsMenu
	public static function adminAddSettingsMenu(array $options = []) {
		// Validating inputs
		if(!isset($options['parent']) || (!is_string($options['parent'])) || ($options['parent'] === '')) $options['parent'] = false;
		if(!isset($options['code']) || (!is_string($options['code'])) || ($options['code'] === '')) \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param option['code']"]);
		if(!isset($options['label']) || (!is_string($options['label'])) || ($options['label'] === '')) \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param option['label']"]);
		if(!isset($options['icon']) || (!is_string($options['icon'])) || ($options['icon'] === '')) $options['icon'] = 'dashicons-admin-generic';
		if(!isset($options['callback']) || (!is_string($options['callback'])) || ($options['callback'] === '')) $options['callback'] = 'initAdminSettingsPage';
		if($options['parent'] === false) {
			// Adding a parent menu item
			add_action('admin_menu', function() use ($options) {
				add_menu_page(
					__($options['label'], _zCfg['guid']),
					__($options['label'], _zCfg['guid']),
					'manage_options',
					$options['code'],
					'\Rou9e\Wordpress\AbstractHelper::initAdminSettingsPage',
					$options['icon'],
					'80.001'
				);
			});
		}
		else {
			// Adding a child menu item
			add_action('admin_menu', function() use ($options) {
				add_submenu_page(
					$options['parent'],
					__($options['label'], _zCfg['guid']),
					__($options['label'], _zCfg['guid']),
					'manage_options',
					$options['code'],
					[static::getClassName(), $options['callback']],
				);
			});
		}
	}

	// #################################################################################################### renderAdminSettingsPage
	public static function renderAdminSettingsPage(array $options = []) {
		// Validating inputs
		if(!isset($options['title']) || (!is_string($options['title']))) $options['title'] = '';
		if(!isset($options['baseline']) || (!is_string($options['baseline']))) $options['baseline'] = '';
		if(!isset($options['intro']) || (!is_string($options['intro']))) $options['intro'] = '';
		if(!isset($options['content']) || (!is_string($options['content']))) $options['content'] = '';
		if(!isset($options['outro']) || (!is_string($options['outro']))) $options['outro'] = '';
		if(!isset($options['acf']) || (!is_bool($options['acf']))) $options['acf'] = false;
		if(!isset($options['acf_code']) || (!is_string($options['acf_code'])) || ($options['acf_code'] === '')) $options['acf_code'] = static::getClassGuid();
		if(!isset($options['acf_html_before']) || (!is_string($options['acf_html_before'])) || ($options['acf_html_before'] === '')) $options['acf_html_before'] = '';
		if(!isset($options['acf_html_after']) || (!is_string($options['acf_html_after'])) || ($options['acf_html_after'] === '')) $options['acf_html_after'] = '';
		if(!isset($options['acf_submit']) || (!is_string($options['acf_submit'])) || ($options['acf_submit'] === '')) $options['acf_submit'] = "Update";
		if(!isset($options['acf_success']) || (!is_string($options['acf_success'])) || ($options['acf_success'] === '')) $options['acf_success'] = "Informations saved";
		// Generating HTML
		$class = _zCfg['generic'].'-settings';
		$html = '<div class="'.$class.' '.$class.'-'.$options['acf_code'].'">';
		if($options['title'] !== '') $html .= '<h1 class="'.$class.'-title">'.$options['title'].'</h1>';
		if($options['baseline'] !== '') $html .= '<p class="'.$class.'-baseline">'.$options['baseline'].'</p>';
		if($options['intro'] !== '') $html .= '<div class="'.$class.'-intro">'.$options['intro'].'</div>';
		if($options['content'] !== '') $html .= '<div class="'.$class.'-content">'.$options['content'].'</div>';
		// Handling ACF form
		if($options['acf'] === true) {
			do_action('acf/input/admin_head');
			do_action('acf/input/admin_enqueue_scripts');
			$html .= '<div class="'.$class.'-acf-form">';
			ob_start();
			acf_form([
				'id'											=> 'acf-form-'.$options['acf_code'],
				'post_id'									=> 'options',
				'new_post'								=> false,
				'html_before_fields'			=> $options['acf_html_before'],
				'html_after_fields'				=> $options['acf_html_after'],
				'label_placement'					=> 'left',
				'field_groups'						=> [$options['acf_code']],
				'return'									=> admin_url('admin.php?page='.$options['acf_code']),
				'submit_value'						=> __($options['acf_submit'], _zCfg['guid']),
				'updated_message'					=> __($options['acf_success'], _zCfg['guid']),
			]);
			$html .= ob_get_clean();
			$html .= '</div>';
		}
		if($options['outro'] !== '')	$html .= '<div class="'.$class.'-outro">'.$options['outro'].'</div>';
		$html .= '</div>';
		// Returning HTML
		echo $html;
	}

	// #################################################################################################### renderFrontPage
	public static function renderFrontPage(array $options = []) {
		// https://wordpress.stackexchange.com/questions/9870/how-do-you-create-a-virtual-page-in-wordpress
		// Validating inputs
		if(!isset($options['url_key']) || (!is_string($options['url_key'])) || ($options['url_key'] === '')) \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param option['url_key']"]);
		if(!isset($options['code']) || (!is_string($options['code'])) || ($options['code'] === '')) \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param option['code']"]);
		if(!isset($options['content']) || (!is_string($options['content']))) $options['content'] = '';
		if(!isset($options['callback']) || (!is_string($options['callback']))) $options['callback'] = '';
		// Lets go
		add_action('wp_loaded', function() use ($options) {
			add_rewrite_rule($options['url_key'].'$', 'index.php?'.$options['code'].'=1', 'top');
		});
		register_activation_hook(__FILE__, function() {
			flush_rewrite_rules(true);
		});
		add_filter('query_vars', function($query_vars) use ($options) {
			$query_vars[] = $options['code'];
			return $query_vars;
		});
		add_action('parse_request', function(& $wp) use ($options) {
			if(array_key_exists($options['code'], $wp->query_vars)) {
				if($wp->query_vars[$options['code']] === '1') {
					if($options['callback'] !== '') call_user_func($options['callback']);
					if($options['content'] !== '') echo $options['content'];
					exit;
				}
			}
			return;
		});
	}

	// #################################################################################################### createAfcForm
	public static function createAfcForm(array $options = []) {
		// Validating ACF form inputs
		if(!isset($options['code']) || (!is_string($options['code'])) || ($options['code'] === '')) $options['code'] = static::getClassGuid();
		if(!isset($options['title']) || (!is_string($options['title'])) || ($options['title'] === '')) $options['title'] = static::getClassGuid();
		if(!isset($options['description']) || (!is_string($options['description'])) || ($options['description'] === '')) $options['description'] = "Used to handle some additional configuration";
		if(!isset($options['options']) || (!is_array($options['options']))) $options['options'] = [];
		if(!isset($options['fields']) || (!is_array($options['fields']))) $options['fields'] = [];
		// Validating ACF form fields
		foreach($options['fields'] as & $field) {
			if(!isset($field['type']) || (!is_string($field['type'])) || ($field['type'] === '')) \Rou9e\Framework\Messages::throwException('NOT_VALID', ['what' => "Param options['fields']['".$field['code']."']['type']"]);
			if(!isset($field['code']) || (!is_string($field['code'])) || ($field['code'] === '')) \Rou9e\Framework\Messages::throwException('NOT_VALID', ['what' => "Param options['fields']['?']['code']"]);
			if(!isset($field['label']) || (!is_string($field['label'])) || ($field['label'] === '')) \Rou9e\Framework\Messages::throwException('NOT_VALID', ['what' => "Param options['fields']['".$field['code']."']['label']"]);
			if(!isset($field['instructions']) || (!is_string($field['instructions']))) $field['instructions'] = '';
			if(!isset($field['required']) || (!is_int($field['required'])) || (!in_array($field['required'], [0, 1]))) $field['required'] = 0;
			if(!isset($field['default_value']) || (!is_string($field['default_value']))) $field['default_value'] = '';
			if(!isset($field['options']) || (!is_array($field['options']))) $field['options'] = [];
		}
		unset($field);
		// Creating ACF form
		if(function_exists('acf_add_local_field_group')) {
			// Preparing ACF form
			$class = _zCfg['generic'].'-acf-form';
			$form = [
				'key' 										=> $options['code'],
				'class'										=> $class.' '.$class.'-'.$options['code'],
				'title'										=> __($options['title'], _zCfg['guid']),
				'description'							=> __($options['description'], _zCfg['guid']),
				'menu_order'							=> 0,
				'position'								=> 'normal',
				'style'										=> 'default',
				'label_placement'					=> 'left',
				'instruction_placement'		=> 'label',
				'hide_on_screen'					=> '',
				'active'									=> true,
			];
			$form = array_merge($form, $options['options']);
			if(!isset($form['location'])) $form['location'] = [[['param' => 'post_type','operator' => '==','value' => md5(microtime(true))]]];
			// Preparing ACF form fields
			$form['fields'] = [];
			foreach($options['fields'] as $item) {
				$field = [
					'key'										=> $options['code'].'-'.$item['code'],
					'name'									=> $options['code'].'-'.$item['code'],
					'label'									=> __($item['label'], _zCfg['guid']),
					'type'									=> $item['type'],
					'instructions'					=> __($item['instructions'], _zCfg['guid']),
					'required'							=> 0,
					'default_value'					=> '',
					'wpml_cf_preferences'		=> 2,
					'message'								=> '',
					'append'								=> '',
					'prepend'								=> '',
					'conditional_logic'			=> 0,
					'maxlength'							=> '',
					'placeholder'						=> '',
					'ui'										=> 0,
					'ui_off_text'						=> '',
					'ui_on_text'						=> '',
					'wrapper'								=> ['width' => '', 'class' => '', 'id' => ''],
				];
				$field = array_merge($field, $item['options']);
				$form['fields'][] = $field;
			}
			// Creating ACF form
			acf_add_local_field_group($form);
		}
	}
}
