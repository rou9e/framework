<?php

namespace Rou9e\Wordpress;

// \Rou9e\Wordpress\GravityForms::extraDataFromForm($entry, $form);

class GravityForms  {

	// ####################################################################################################
	public static function extraDataFromForm($entry, $form) {
		$formData = [];
		$formData['site_url']							= \Rou9e\Wordpress\Core::getRootUrl();
		$formData['lead_url']							= $entry['source_url'];
		$formData['lead_ip']							= $entry['ip'];
		$formData['lead_date']						= $entry['date_created'];
		$formData['form_id']							= $entry['form_id'];
		$formData['form_title']						= $form['title'];
		$formData['form_fields']					= [];
		// Handling fields
		foreach($form['fields'] as $field) {
			if(@$field['adminLabel'] != null) {
				$item = [];
				$item['field_id']							= $field['id'];
				$item['label']								= $field['label'];
				$item['name']									= $field['adminLabel'];
				$item['type']									= $field['type'];
				$item['value']								= (@$entry[$field['id']] !== null) ? $entry[$field['id']] : '';
				if($item['type'] === 'multiselect') {
					if(\Rou9e\Framework\Str::isJson($item['value'])) $item['value'] = json_decode($item['value']);
					if($item['value'] == '') $item['value'] = [];
				}
				if($item['type'] === 'checkbox') {
					$selected = [];
					foreach($field['inputs'] as $choice) {
						$selected[] = (@$entry[$choice['id']] !== null) ? $entry[$choice['id']] : '';
					}
					$item['value'] = array_filter($selected);
					if($item['value'] == '') $item['value'] = [];
				}
				$formData['form_fields'][$field['adminLabel']] = $item;
			}
		}
		// Handling fields values
		$formData['form_values']					= [];
		foreach($formData['form_fields'] as $field) {
			$formData['form_values'][$field['name']] = $field['value'];
		}
		return $formData;
	}
}
