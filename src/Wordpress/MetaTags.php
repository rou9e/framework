<?php

namespace Rou9e\Wordpress;

// \Rou9e\Wordpress\MetaTags::isModuleEnabled();

class MetaTags extends \Rou9e\Wordpress\AbstractHelper {

	// #################################################################################################### initExtension
	public static function initExtension() {
		parent::initExtension();
		// Overrinding meta_title
		if(\Rou9e\Wordpress\MetaTags::isModuleEnabled() === true) {
			add_filter('pre_get_document_title', function() {
				return \Rou9e\Wordpress\MetaTags::getMetaTitle();
			}, PHP_INT_MAX, 1);
		}
		// This would work but could cause multiple meta
		if(\Rou9e\Wordpress\MetaTags::isModuleEnabled() === true) {
			add_action('wp_head', function() {
				$metaKeywords = \Rou9e\Wordpress\MetaTags::getMetaKeywords();
				if($metaKeywords != false || $metaKeywords != '') {
					echo '<meta name="keywords" content="'.$metaKeywords.'"><!-- Custom -->'."\n";
				}
				$metaDescription = \Rou9e\Wordpress\MetaTags::getMetaDescription();
				if($metaDescription != false || $metaDescription != '') {
					echo '<meta name="description" content="'.$metaDescription.'"><!-- Custom -->'."\n";
				}
			}, PHP_INT_MAX, 1);
		}
		// NoIndex / NoFollow meta
		if(\Rou9e\Wordpress\MetaTags::isModuleEnabled() === true) {
			add_action('wp_head', function() {
				$postTypes = \Rou9e\Wordpress\MetaTags::getNoIndexNoFollowPostTypes();
				if(count($postTypes) > 0) {
					$postType = get_post_type(get_queried_object_id());
					if(in_array($postType, $postTypes)) {
						echo '<meta name="robots" content="noindex, nofollow" /><!-- Custom -->'."\n";
					}
				}
				if(\Rou9e\Wordpress\MetaTags::isNoIndexNoFollowMetaTagEnabled() === true) {
					echo '<meta name="robots" content="noindex, nofollow" /><!-- Custom -->'."\n";
				}
			});
		}
	}

	// #################################################################################################### initAdminSettingsMenu
	public static function initAdminSettingsMenu() {
		static::adminAddSettingsMenu([
			'parent'		=> static::getGuid().'_container',
			'code'			=> static::getClassGuid(),
			'label'			=> static::getClassShortName(),
		]);
	}

	// #################################################################################################### initAdminSettingsPage
	public static function initAdminSettingsPage() {
		$outro = '<div id="'.static::getClassGuid().'">';
		$outro .= 	'<style>';
		$outro .= 		'#'.static::getClassGuid().'-meta_title-generator{cursor:pointer;font-weight:bold;}';
		$outro .= 	'</style>';
		$outro .= 	'<script type="text/javascript">';
		$outro .= 		'jQuery(document).ready(function() {';
		$outro .= 			'if(jQuery("#acf-'.static::getClassGuid().'-meta_title").length > 0) {';
		$outro .= 				'if(jQuery("#editor").length === 0) {';
		$outro .= 					'jQuery("#'.static::getClassGuid().'-meta_title-generator").parent().hide();';
		$outro .= 				'}';
		$outro .= 				'setTimeout(function() {';
		$outro .= 					'if(jQuery(".editor-post-title__input").length === 0) {';
		$outro .= 						'jQuery("#'.static::getClassGuid().'-meta_title-generator").parent().hide();';
		$outro .= 					'}';
		$outro .= 				'}, 2000);';
		$outro .= 				'jQuery(document).on("click", "#'.static::getClassGuid().'-meta_title-generator", function(event) {';
		$outro .= 					'if(confirm("Are you sure you want to replace the current meta_title ?")) {';
		$outro .= 						'jQuery("#acf-'.static::getClassGuid().'-meta_title").val(jQuery(".editor-post-title__input").val());';
		$outro .= 					'}';
		$outro .= 				'});';
		$outro .= 			'}';
		$outro .= 		'});';
		$outro .= 	'</script>';
		$outro .= '</div>';
		static::renderAdminSettingsPage([
			'title'										=> ucfirst(static::getGuid()).' '.static::getClassShortName(),
			'description'							=> "Here you can setup some additional configuration. Remember to setup the configuration for every language if you are using WPML.",
			'acf'											=> true,
			'outro'										=> $outro,
		]);
	}

	// #################################################################################################### initAcfGroupAndFields
	public static function initAcfGroupAndFields() {
		// Defining where the meta fields will be displayed based on configuration
		$postTypes = \Rou9e\Wordpress\MetaTags::getIncludedPosts();
		if(in_array($postTypes, [null, false, ''])) $postTypes = ['post', 'page'];
		if(!is_array($postTypes)) $postTypes = ['post', 'page'];
		$locations = [['param' => 'post_type', 'operator' => '!=', 'value' => md5(microtime(true))]];
		$locations = [];
		foreach($postTypes as $postType) $locations[] = [['param' => 'post_type', 'operator' => '==', 'value' => $postType]];
		// Creating ACF Form
		static::createAfcForm([
			'options'									=> ['location' => $locations],
			'fields'									=> [
				[
					'type'								=> "true_false",
					'code'								=> 'enabled',
					'label'								=> "Enabled",
					'instructions'				=> "Enable / Disable the meta tag substitution",
					'default_value'				=> 0,
				],
				[
					'type'								=> "text",
					'code'								=> 'meta_title',
					'label'								=> "meta_title",
					'instructions'				=> "Used as default meta_title. Let empty if you want to use post_title",
					'options'							=> ['append' => '<span id="'.static::getClassGuid().'-meta_title-generator">🗲</span>'],
				],
				[
					'type'								=> "text",
					'code'								=> 'meta_keywords',
					'label'								=> "meta_keywords",
					'instructions'				=> "Used as default meta_keywords",
				],
				[
					'type'								=> "textarea",
					'code'								=> 'meta_description',
					'label'								=> "meta_description",
					'instructions'				=> "Used as default meta_description",
					'options'							=> ['rows' => 3],
				],
				[
					'type'								=> "text",
					'code'								=> 'meta_title_prefix',
					'label'								=> "meta_title_prefix",
					'instructions'				=> "Used as meta_title prefix",
				],
				[
					'type'								=> "text",
					'code'								=> 'meta_title_suffix',
					'label'								=> "meta_title_suffix",
					'instructions'				=> "Used as meta_title suffix",
				],
				[
					'type'								=> "select",
					'code'								=> 'noindex_nofollow_post_types',
					'label'								=> "NoIndex / NoFollow post_types",
					'instructions'				=> "Select the post_types that will display noindex,nofollow meta",
					'options'							=> [
						'choices'						=> \Rou9e\Wordpress\Core::getCleanedPostTypesList(),
						'default_value'			=> [],
						'allow_null'				=> 1,
						'multiple'					=> 1,
						'return_format'			=> 'value',
						'ajax'							=> 0,
					],
				],
				[
					'type'								=> "select",
					'code'								=> 'include_post_types',
					'label'								=> "Include post_types",
					'instructions'				=> "Select the post_types that will display meta fields in the backend",
					'options'							=> [
						'choices'						=> \Rou9e\Wordpress\Core::getCleanedPostTypesList(),
						'default_value'			=> ['post', 'page'],
						'allow_null'				=> 0,
						'multiple'					=> 1,
						'return_format'			=> 'value',
						'ajax'							=> 0,
					],
				],
				[
					'type'								=> "true_false",
					'code'								=> 'noindex_nofollow',
					'label'								=> "meta_noindex/nofollow",
					'instructions'				=> "Add noindex / nofollow meta tag",
					'default_value'				=> 0,
				],
			],
		]);
	}

	// #################################################################################################### initInjectHtmlInAdminFooter
	public static function initInjectHtmlInAdminFooter() {
		$html = '';
		$html .= '<div id="'.static::getClassGuid().'">';
		$html .= 	'<style>';
		$html .= 		'body.wp-admin:not(.zFramework-settings-page-'.static::getClassGuid().') .acf-field[data-key="'.static::getClassGuid().'-enabled"]{display:none !important;}';
		$html .= 		'body.wp-admin:not(.zFramework-settings-page-'.static::getClassGuid().') .acf-field[data-key="'.static::getClassGuid().'-meta_title_prefix"]{display:none !important;}';
		$html .= 		'body.wp-admin:not(.zFramework-settings-page-'.static::getClassGuid().') .acf-field[data-key="'.static::getClassGuid().'-meta_title_suffix"]{display:none !important;}';
		$html .= 		'body.wp-admin:not(.zFramework-settings-page-'.static::getClassGuid().') .acf-field[data-key="'.static::getClassGuid().'-include_post_types"]{display:none !important;}';
		$html .= 		'body.wp-admin:not(.zFramework-settings-page-'.static::getClassGuid().') .acf-field[data-key="'.static::getClassGuid().'-include_post_types"]{display:none !important;}';
		$html .= 		'body.wp-admin:not(.zFramework-settings-page-'.static::getClassGuid().') .acf-field[data-key="'.static::getClassGuid().'-noindex_nofollow_post_types"]{display:none !important;}';
		$html .= 		'body.wp-admin.zFramework-settings-page-'.static::getClassGuid().' .acf-field[data-key="'.static::getClassGuid().'-noindex_nofollow"]{display:none !important;}';
		$html .= 	'</style>';
		$html .= '</div>';
		echo $html;
	}

	// ####################################################################################################
	public static function getMetaTitleWithoutPrefixNorSuffix() {
		$value = \Rou9e\Wordpress\Acf::getAcfValue(static::getClassGuid().'-meta_title', '');
		if($value === '') $value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-meta_title', '');
		$object = get_queried_object();
		if($value == '' && $object != null && property_exists($object, 'post_title') && $object->post_title != null && $object->post_title != '') $value = $object->post_title;
		return $value;
	}

	// ####################################################################################################
	public static function getMetaTitlePrefix() {
		$value = \Rou9e\Wordpress\Acf::getAcfValue(static::getClassGuid().'-meta_title_prefix', '');
		if($value === '') $value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-meta_title_prefix', '');
		return $value;
	}

	// ####################################################################################################
	public static function getMetaTitleSuffix() {
		$value = \Rou9e\Wordpress\Acf::getAcfValue(static::getClassGuid().'-meta_title_suffix', '');
		if($value === '') $value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-meta_title_suffix', '');
		return $value;
	}

	// ####################################################################################################
	public static function getMetaTitle() {
		$prefix = \Rou9e\Wordpress\MetaTags::getMetaTitlePrefix();
		$metaTitle = \Rou9e\Wordpress\MetaTags::getMetaTitleWithoutPrefixNorSuffix();
		$suffix = \Rou9e\Wordpress\MetaTags::getMetaTitleSuffix();
		return $prefix.$metaTitle.$suffix;
	}

	// ####################################################################################################
	public static function getMetaDescription() {
		$value = \Rou9e\Wordpress\Acf::getAcfValue(static::getClassGuid().'-meta_description', '');
		if($value === '') $value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-meta_description', '');
		return $value;
	}

	// ####################################################################################################
	public static function getMetaKeywords() {
		$value = \Rou9e\Wordpress\Acf::getAcfValue(static::getClassGuid().'-meta_keywords', '');
		if($value === '') $value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-meta_keywords', '');
		return $value;
	}

	// ####################################################################################################
	public static function getIncludedPosts() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-include_post_types');
		return $value;
	}

	// #################################################################################################### isModuleEnabled
	public static function isNoIndexNoFollowMetaTagEnabled(): bool {
		$value = \Rou9e\Wordpress\Acf::getAcfBooleanValue(static::getClassGuid().'-noindex_nofollow', false);
		if(is_bool($value)) return $value;
		return false;
	}

	// ####################################################################################################
	public static function getNoIndexNoFollowPostTypes() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-noindex_nofollow_post_types');
		if($value == '') return [];
		return $value;
	}
}