<?php

namespace Rou9e\Wordpress;

// \Rou9e\Wordpress\Acf::getAcfField('xxx', 'option');

class Acf extends \Rou9e\Wordpress\AbstractHelper {

	// #################################################################################################### getAcfOptionValue
	public static function getAcfOptionValue(string $field = '', $default = null, $language = null) {
		if($field === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param 'field'"]);
		// Initiating the translation key
		$key = $field;
		// Adding if needed the language to the translation key
		if(\Rou9e\Wordpress\Wpml::isWpmlInstalled()) {
			if($language !== false) {
				if($language === null) {
					$language = @\Rou9e\Wordpress\Wpml::getCurrentLanguageCode() ?? '';
					if($language !== '' && $language !== \Rou9e\Wordpress\Wpml::getDefaultLanguage()) {
						$key = $language.'_'.$key;
					}
				}
			}
		}
		// Completing the translation key
		$key = 'options_'.$key;
		// Returning the result
		return get_option($key, $default);
	}

	// #################################################################################################### getAcfOptionBooleanValue
	public static function getAcfOptionBooleanValue(string $field = '', $default = null, $language = null) {
		$value = static::getAcfOptionValue($field, $default, $language);
		if($value === 1 || $value === '1' || $value === true) return true;
		if($value === 0 || $value === '0' || $value === false) return false;
		\Rou9e\Wordpress\Core::log("Field '".$field."' is not a boolean");
		return $default;
	}

	// #################################################################################################### getAcfOptionDateValue
	public static function getAcfOptionDateValue(string $field = '', $default = null, $language = null, $format = 'd.m.Y') {
		$value = static::getAcfOptionValue($field, $default, $language);
		$value .= '';
		$date = \DateTime::createFromFormat('Ymd', $value);
		return $date->format($format);
	}

	// #################################################################################################### getAcfOptionJsonValue
	public static function getAcfOptionJsonValue(string $field = '', $default = null, $language = null) {
		$value = static::getAcfOptionValue($field, $default, $language);
		$value .= '';
		return \Rou9e\Framework\Core::jsonDecode($value);
	}

	// #################################################################################################### getAcfValue
	public static function getAcfValue(string $field = '', $default = null, $what = null) {
		if($what === null) $what = get_queried_object_id();
		$value = get_field($field, $what, false);
		if($value == null) return $default;
		return $value;
	}

	// #################################################################################################### getAcfBooleanValue
	public static function getAcfBooleanValue(string $field = '', $default = null, $what = null) {
		$value = static::getAcfValue($field, $default, $what);
		if($value === 1 || $value === '1' || $value === true) return true;
		if($value === 0 || $value === '0' || $value === false) return false;
		\Rou9e\Wordpress\Core::log("Field '".$field."' is not a boolean");
		return $default;
	}

	// #################################################################################################### getAcfDateValue
	public static function getAcfDateValue(string $field = '', $default = null, $language = null, $format = 'd.m.Y') {
		$value = static::getAcfValue($field, $default, $what);
		$value .= '';
		$date = \DateTime::createFromFormat('Ymd', $value);
		return $date->format($format);
	}

	// #################################################################################################### getAcfDateValue
	public static function getAcfJsonValue(string $field = '', $default = null, $language = null, $format = 'd.m.Y') {
		$value = static::getAcfValue($field, $default, $what);
		$value .= '';
		return \Rou9e\Framework\Core::jsonDecode($value);
	}

	// #################################################################################################### getAcfImageValue
	public static function getAcfImageValue(string $field = '', $default = null, $what = null) {
		if($what === null) $what = get_queried_object_id();
		$value = get_field($field, $what);
		if($value == null) return $default;
		return $value;
	}

	// #################################################################################################### getAcfLinkValue
	public static function getAcfLinkValue(string $field = '', $default = null, $what = null) {
		if($what === null) $what = get_queried_object_id();
		$value = get_field($field, $what);
		if($value == null) return $default;
		return $value;
	}
}
