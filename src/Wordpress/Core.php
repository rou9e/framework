<?php

namespace Rou9e\Wordpress;

// \Rou9e\Wordpress\Core::getRootUrl();

class Core extends \Rou9e\Wordpress\AbstractHelper {

	// #################################################################################################### initExtension
	public static function initExtension() {
		// Calling parent function
		parent::initExtension();
		// Adding CSS / JS assets to the admin
		add_action('admin_enqueue_scripts', function() {
			wp_enqueue_style(static::getGuid().'-framework-css', \Rou9e\Wordpress\Core::getFrameworkUrl().'pub/css/zframework.css');
			wp_enqueue_script(static::getGuid().'-framework-js', \Rou9e\Wordpress\Core::getFrameworkUrl().'pub/js/zframework.js');
		});
		// Adding CSS classes to body
		static::adminAddCssClassToBody('zFramework');
		// Adding settings menu container
		static::adminAddSettingsMenu([
			'code'			=> static::getGuid().'_container',
			'label'			=> ucfirst(static::getGuid())
		]);
		// Overriding search url
		add_action('template_redirect', '\Rou9e\Wordpress\Core::overrideSearchUrl');
	}

	// #################################################################################################### initAdminSettingsMenu
	public static function initAdminSettingsMenu() {
		static::adminAddSettingsMenu([
			'parent'							=> static::getGuid().'_container',
			'code'								=> static::getClassGuid(),
			'label'								=> static::getClassShortName(),
		]);
	}

	// #################################################################################################### initAdminSettingsPage
	public static function initAdminSettingsPage() {
		static::renderAdminSettingsPage([
			'title'								=> ucfirst(static::getGuid()).' '.static::getClassShortName(),
			'description'					=> "Here you can setup some additional configuration. Remember to setup the configuration for every language if you are using WPML.",
			'acf'									=> true,
		]);
	}

	// #################################################################################################### initAcfGroupAndFields
	public static function initAcfGroupAndFields() {
		static::createAfcForm([
			'fields'						=> [
				[
					'type'						=> "true_false",
					'code'						=> 'hide_wpml_duplicate_actions',
					'label'						=> "Hide WPML duplicate actions",
					'instructions'		=> "Hide the (messy) duplicate checkbox",
					'default_value'		=> 1,
				],
				[
					'type'						=> "text",
					'code'						=> 'override_search_url',
					'label'						=> "Override search url_key",
					'instructions'		=> "Leave the field empty to disable",
					'default_value'		=> 'search',
				],
			],
		]);
	}

	// #################################################################################################### initInjectHtmlInAdminFooter
	public static function initInjectHtmlInAdminFooter() {
		$html = '';
		$html .= '<div id="'.static::getClassGuid().'">';
		$html .= 	'<style>';
		$html .= 		'body.wp-admin ul#adminmenu li.menu-top.toplevel_page_alioze_container > ul.wp-submenu > li.wp-first-item > a.wp-first-item {display:none !important;}';
		if(\Rou9e\Wordpress\Acf::getAcfOptionBooleanValue(static::getClassGuid().'-hide_wpml_duplicate_actions', false) !== true) {
			$html .= 		'body.wp-admin #icl_translate_options input[type="checkbox"][name="icl_dupes[]"]:not(.osef){display:inline-block !important;}';
		}
		$html .= 	'</style>';
		$html .= '</div>';
		echo $html;
	}

	// #################################################################################################### overrideSearchUrl
	public static function overrideSearchUrl() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-override_search_url', '');
		if(is_string($value)) {
			$value = trim($value);
			if(!empty($value)) {
				if(is_search() && !empty($_GET['s'])) {
					wp_redirect(home_url("/search/").urlencode(get_query_var('s'))); exit;
				}
			}
		}
	}

	// #################################################################################################### getCleanedPostTypesList
	public static function getCleanedPostTypesList(): array {
		$postTypes = get_post_types();
		$postTypes['web-story'] = 'web-story';
		$postTypes['shb_accommodation'] = 'shb_accommodation';
		$postTypes['testimonial'] = 'testimonial';
		unset($postTypes['acf-field']);
		unset($postTypes['acf-field-group']);
		unset($postTypes['acf-post-type']);
		unset($postTypes['acf-taxonomy']);
		unset($postTypes['attachment']);
		unset($postTypes['cookielawinfo']);
		unset($postTypes['custom_css']);
		unset($postTypes['customize_changeset']);
		unset($postTypes['elementor_font']);
		unset($postTypes['elementor_icons']);
		unset($postTypes['elementor_library']);
		unset($postTypes['elementor_snippet']);
		unset($postTypes['nav_menu_item']);
		unset($postTypes['oembed_cache']);
		unset($postTypes['revision']);
		unset($postTypes['user_request']);
		unset($postTypes['wp_block']);
		unset($postTypes['wp_global_styles']);
		unset($postTypes['wp_navigation']);
		unset($postTypes['wp_template']);
		unset($postTypes['wp_template_part']);
		return $postTypes;
	}

	// #################################################################################################### getPostType()
	public static function getPostType($what = null): string {
		if($what === null) $what = get_queried_object_id();
		return get_post_type($what);
	}

	// #################################################################################################### isPage()
	public static function isHome(): bool {
		return is_home();
	}

	// #################################################################################################### isPage()
	public static function isPage($what = null): bool {
		if($what === null) $what = get_queried_object_id();
		return (static::getPostType() === 'page');
	}

	// #################################################################################################### isPost()
	public static function isPost($what = null): bool {
		if($what === null) $what = get_queried_object_id();
		return (static::getPostType() === 'post');
	}

	// #################################################################################################### https://domain.com/
	public static function getRootUrl(): string {
		return rtrim(get_site_url(), '/').'/';
	}

	// #################################################################################################### https://domain.com/{xx?}
	public static function getBaseUrl(): string {
		return \Rou9e\Wordpress\Core::getHomeUrl();
	}

	// #################################################################################################### https://domain.com/{xx?}
	public static function getHomeUrl(): string {
		return rtrim(get_home_url(), '/').'/';
	}

	// #################################################################################################### https://domain.com/{blog?}
	public static function getBlogUrl(): string {
		return rtrim(get_permalink(get_option('page_for_posts')), '/');
	}

	// #################################################################################################### https://domain.com/wp-admin/
	public static function getAdminUrl(): string {
		return rtrim(get_admin_url(), '/').'/';
	}

	// #################################################################################################### https://domain.com/wp-content/themes/my-parent-theme/
	public static function getParentThemeUrl(): string {
		return rtrim(get_template_directory_uri(), '/').'/';
	}

	// #################################################################################################### https://domain.com/wp-content/themes/my-child-theme/
	public static function getThemeUrl(): string {
		return rtrim(get_stylesheet_directory_uri(), '/').'/';
	}

	// #################################################################################################### https://domain.com/wp-content/themes/my-child-theme/style.css
	public static function getThemeStyleUrl(): string {
		return get_stylesheet_uri();
	}

	// #################################################################################################### https://domain.com/wp-content/plugins/
	public static function getPluginsUrl(): string {
		return rtrim(plugins_url(), '/').'/';
	}

	// #################################################################################################### https://domain.com/wp-content/plugins/framework-guid/
	public static function getFrameworkUrl(): string {
		return \Rou9e\Wordpress\Core::getPluginsUrl().'framework-'.static::getGuid().'/';
	}

	// #################################################################################################### https://domain.com/wp-content/themes/
	public static function getThemesUrl(): string {
		return rtrim(get_theme_root_uri(), '/').'/';
	}

	// #################################################################################################### slug-url
	public static function getSlugUrl(string $url = '', bool $trimmed = true): string {
		if($url === '') $url = \Rou9e\Framework\Core::getCurrentUrl();
		if($trimmed === false) return str_replace(\Rou9e\Wordpress\Core::getBaseUrl(), '', $url);
		if($trimmed === true) return trim(str_replace(\Rou9e\Wordpress\Core::getBaseUrl(), '', $url), ' /');
	}

	// #################################################################################################### /home/xxx/www/projects/domain.com/prod/
	public static function getBasePath(): string {
		return rtrim(get_home_path(), '/').'/';
	}

	// #################################################################################################### /home/xxx/www/projects/domain.com/prod/wp-content/plugins
	public static function getPluginsPath(): string {
		return rtrim(WP_PLUGIN_DIR, '/').'/';
	}

	// #################################################################################################### /home/xxx/www/projects/domain.com/prod/wp-content/plugins/framework-guid/
	public static function getFrameworkPath(): string {
		return \Rou9e\Wordpress\Core::getPluginsPath().'framework-'.static::getGuid().'/';
	}

	// #################################################################################################### /home/xxx/www/projects/domain.com/prod/wp-content/themes
	public static function getThemesPath(): string {
		return rtrim(get_theme_root(), '/').'/';
	}

	// #################################################################################################### /home/xxx/www/projects/domain.com/prod/wp-content/themes/my-parent-theme/
	public static function getParentThemePath(): string {
		return rtrim(get_template_directory(), '/').'/';
	}

	// #################################################################################################### /home/xxx/www/projects/domain.com/prod/wp-content/themes/my-child-theme/
	public static function getThemePath(): string {
		return rtrim(get_stylesheet_directory(), '/').'/';
	}

	// #################################################################################################### getPostsCategories
	public static function getPostsCategories(array $options = ['hide_empty' => 0]): array {
		$categories = get_categories($options);
		return $categories;
	}

	// #################################################################################################### getTagsCategories
	public static function getPostsTags(array $options = ['hide_empty' => 0]): array {
		$tags = get_tags($options);
		return $tags;
	}

	// #################################################################################################### getTextWidget
	public static function getTextWidget(string $identifier = ''): string {
		$html = '';
		ob_start();
			dynamic_sidebar($identifier);
		$html = ob_get_clean();
		// Deleting the "before"
		$html = str_replace('<div class="textwidget">', '', $html);
		// Deleting the "after"
		$html = strrev(implode(strrev(''), explode('>vid/<', strrev($html), 2)));
		// Trimming
		$html = trim($html);
		return $html;
	}

	// #################################################################################################### log
	public static function log($mixed = '', array $options = []) {
		// Validating input
		if(!isset($options['prefix']) || !is_string($options['prefix'])) $options['prefix'] = '';
		if(!isset($options['date']) || !is_bool($options['date'])) $options['date'] = true;
		// Preparing data
		$content = $mixed;
		if(is_array($content) || is_object($content)) $content = \Rou9e\Framework\Core::dump($content, ['mode' => 'text', 'return' => true]);
		if($options['date']) $content = \Rou9e\Framework\Core::getFormattedDate().' // '.$content;
		$content = $options['prefix'].$content;
		$content = $content."\n";
		// Writing file
		error_log($content);
	}
}
