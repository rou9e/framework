<?php

namespace Rou9e\Wordpress;

// \Rou9e\Wordpress\Pipedrive::isModuleEnabled();

class Pipedrive extends \Rou9e\Wordpress\AbstractHelper {

	CONST DEBUG = false;

	// #################################################################################################### initExtension
	public static function initExtension() {
		parent::initExtension();
	}

	// #################################################################################################### initHooks
	public static function initHooks() {
		static::registerUsedPluginHook();
	}

	// #################################################################################################### initAdminSettingsMenu
	public static function initAdminSettingsMenu() {
		static::adminAddSettingsMenu([
			'parent'									=> static::getGuid().'_container',
			'code'										=> static::getClassGuid(),
			'label'										=> static::getClassShortName(),
		]);
	}

	// #################################################################################################### initAdminSettingsPage
	public static function initAdminSettingsPage() {
		static::renderAdminSettingsPage([
			'title'										=> ucfirst(static::getGuid()).' '.static::getClassShortName(),
			'description'							=> "Here you can setup some additional configuration. Remember to setup the configuration for every language if you are using WPML.",
			'acf'											=> true,
		]);
	}

	// #################################################################################################### initAcfGroupAndFields
	public static function initAcfGroupAndFields() {
		static::createAfcForm([
			'fields'									=> [
				[
					'type'								=> "true_false",
					'code'								=> 'enabled',
					'label'								=> "Enabled",
					'instructions'				=> "Enable / Disable the Pipedrive integration",
					'default_value'				=> 1,
				],
				[
					'type'								=> "text",
					'code'								=> 'url',
					'label'								=> "Url",
					'instructions'				=> "The url used in API requests (available in your Pipedrive account)",
					'options'							=> ['placeholder' => 'Example : https://example.pipedrive.com/api/'],
				],
				[
					'type'								=> "text",
					'code'								=> 'token',
					'label'								=> "Token",
					'instructions'				=> "The token used in API requests (available in your Pipedrive account)",
					'options'							=> ['placeholder' => 'Example : 7f0e1fe1d352c81e5794c9758255fe1c2d499123'],
				],
				[
					'type'								=> "select",
					'code'								=> 'lead_label',
					'label'								=> "Lead label",
					'instructions'				=> "The lead label used in API requests (fill token / url first and save)",
					'options'							=> [
						'choices'						=> static::getLeadLabelFromPipedrive(),
						'default_value'			=> [],
						'multiple'					=> 0,
						'return_format'			=> 'value',
						'ajax'							=> 0,
					],
				],
				[
					'type'								=> "text",
					'code'								=> 'emergency',
					'label'								=> "Emergency",
					'instructions'				=> "The emails notificed in case of failure (no space, comma separated)",
					'options'							=> ['placeholder' => 'Example : emergency@'.static::getGuid().'.com,personal@client.com'],
				],
			],
		]);
	}

	// ####################################################################################################
	public static function getUrl() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-url');
		if($value != '') $value = \Rou9e\Framework\Str::finish($value, '/');
		return $value;
	}

	// ####################################################################################################
	public static function getToken() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-token');
		return $value;
	}

	// ####################################################################################################
	public static function getLeadLabel() {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-lead_label');
		return $value;
	}

	// ####################################################################################################
	public static function getEmergencyEmails(): array {
		$value = \Rou9e\Wordpress\Acf::getAcfOptionValue(static::getClassGuid().'-emergency');
		$value = str_replace(' ', '', $value);
		if($value === '') $value = 'emergency@'.static::getGuid().'.com';
		$value = explode(',', $value);
		return $value;
	}

	// ####################################################################################################
	public static function getLeadLabelFromPipedrive(): array {
		// https://xxxxx.pipedrive.com/api/v1/leadLabels?api_token=
		$url = static::getUrl();
		$token = static::getToken();
		if($url == '' || $token == '') return [];
		$client = new \GuzzleHttp\Client();
		$response = $client->request('GET', $url.'v1/leadLabels?api_token='.$token, [
			'http_errors'									=> false,
			'connect_timeout'							=> 30.0,
			'timeout'											=> 30.0,
		]);
		if(!in_array($response->getStatusCode(), [200, 201])) return [];
		$responseBody = json_decode($response->getBody(), true);
		$success = @$responseBody['success'] ?? false;
		$data = @$responseBody['data'] ?? false;
		if($success === false || $data === false) return [];
		$labels = ['' => ''];
		foreach($data as $label) {
			$labels[$label['id']] = $label['name'];
		}
		return $labels;
	}

	// #################################################################################################### getUsedPluginFromConfiguration
	public static function getUsedPluginFromConfiguration(): string {
		if(!\Rou9e\Framework\Arr::hasDot(_zCfg, 'extensions.Pipedrive.configuration.plugin')) \Rou9e\Framework\Messages::throwException('IS_MISSING', ['what' => 'extensions.Pipedrive.configuration.plugin']);
		return _zCfg['extensions']['Pipedrive']['configuration']['plugin'];
	}

	// #################################################################################################### getUsedPluginFromConfiguration
	public static function registerUsedPluginHook() {
		switch(static::getUsedPluginFromConfiguration()) {
			case 'ContactForm7':
				add_action('wpcf7_mail_sent', [static::getClassName(), 'registerContactForm7Hook']);
			break;
			case 'Elementor':
				add_action('elementor_pro/forms/actions/register', [static::getClassName(), 'registerElementorHook']);
			break;
			case 'GravityForms':
				add_action('gform_after_submission', [static::getClassName(), 'registerGravityFormsHook'], 1000, 2);
			break;
			default:
				\Rou9e\Framework\Messages::throwException('NOT_VALID', ['what' => 'extensions.Pipedrive.configuration.plugin']);
			break;
		}
	}

	// #################################################################################################### registerContactForm7Hook
	public static function registerContactForm7Hook($form) {
		\Rou9e\Wordpress\Pipedrive\ContactForm7Hook::run($form);
	}

	// #################################################################################################### registerElementorHook
	public static function registerElementorHook($form_actions_registrar) {
		$form_actions_registrar->register(new \Rou9e\Wordpress\Pipedrive\ElementorHook());
	}

	// #################################################################################################### registerGravityForms
	public static function registerGravityFormsHook($entry, $form) {
		\Rou9e\Wordpress\Pipedrive\GravityFormsHook::run($entry, $form);
	}

	// ####################################################################################################
	public static function getRequestData(array $formData = []): array {
		// Initiating
		$data = [];
		$data['domain']											= static::getUrl();
		$data['token']											= static::getToken();
		$data['lead_label']									= static::getLeadLabel();
		$data['emails']											= static::getEmergencyEmails();
		foreach($data as $k => & $item) {
			if(is_string($item)) $item = trim($item);
			if($item === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => 'Configuration field "'.$k.'"']);
		}
		unset($item);
		// Validating form fields
		$data['formdata']										= [];
		$data['formdata']['firstname']			= @$formData['firstname'] ?? '';
		$data['formdata']['lastname']				= @$formData['lastname'] ?? '';
		$data['formdata']['email']					= @$formData['email'] ?? '';
		$data['formdata']['telephone']			= @$formData['telephone'] ?? '';
		$data['formdata']['message']				= @$formData['message'] ?? '';
		foreach($data['formdata'] as $k => & $item) {
			if(is_string($item)) $item = trim($item);
			if($item === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => 'Form field "'.$k.'"']);
		}
		unset($item);
		// Initiating requests
		$data['requests']											= [
			'person'														=> [
				'docs'														=> 'https://developers.pipedrive.com/docs/api/v1/Persons#addPerson',
				'endpoint'												=> 'v1/persons',
				'verb'														=> 'POST',
				'response'												=> [],
				'payload'													=> [
					'name'													=> $data['formdata']['firstname'].' '.$data['formdata']['lastname'],
					'email'													=> ['value' => $data['formdata']['email']],
					'phone'													=> ['value' => $data['formdata']['telephone']],
				],
			],
			'lead'															=> [
				'docs'														=> 'https://developers.pipedrive.com/docs/api/v1/Leads#addLead',
				'endpoint'												=> 'v1/leads',
				'verb'														=> 'POST',
				'response'												=> [],
				'payload'													=> [
					'title'													=> "Wordpress - ".$data['formdata']['firstname'].' '.$data['formdata']['lastname'],
					'person_id'											=> '',
					'label_ids'											=> [$data['lead_label']],
				],
			],
			'note'															=> [
				'docs'														=> 'https://developers.pipedrive.com/docs/api/v1/Notes#addNote',
				'endpoint'												=> 'v1/notes',
				'verb'														=> 'POST',
				'response'												=> [],
				'payload'													=> [
					'lead_id'												=> '',
					'content'												=> "<strong>Contact Form Message : </strong><br/><br/>".htmlentities($data['formdata']['message']).'<br>',
				],
			],
		];
		return $data;
	}

	// ####################################################################################################
	public static function makeApiRequest(array $data = []): array {
		$domain = @$data['domain'] ?? '';
		$endpoint = @$data['endpoint'] ?? '';
		$token = @$data['token'] ?? '';
		$verb = @$data['verb'] ?? '';
		$payload = @$data['payload'] ?? [];
		if($domain === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => 'Param "domain"']);
		if($endpoint === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => 'Param "endpoint"']);
		if($verb === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => 'Param "verb"']);
		if($token === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => 'Param "token"']);
		if(in_array($verb, ['POST', 'PUT']) && (count($payload) <= 0)) \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => 'Param "payload"']);
		// Executing
		$client = new \GuzzleHttp\Client();
		$response = $client->request($verb, $domain.$endpoint.'?api_token='.$token, [
			'http_errors'									=> false,
			'connect_timeout'							=> 30.0,
			'timeout'											=> 30.0,
			'json'												=> $payload
		]);
		return ['status' => $response->getStatusCode(), 'body' => json_decode($response->getBody(), true)];
	}

	// ####################################################################################################
	public static function makeApiRequestPerson(array $data = []): array {
		$requestData = array_merge($data['requests']['person'], ['domain' => $data['domain'], 'token' => $data['token']]);
		$data['requests']['person']['response'] = static::makeApiRequest($requestData);
		// Checking HTTP code
		if(!in_array($data['requests']['person']['response']['status'], [200, 201])) \Rou9e\Framework\Messages::throwException('CREATE_FAILURE', ['what' => 'Entity "Person"']);
		// Checking 'success' field
		if($data['requests']['person']['response']['body']['success'] !== true) \Rou9e\Framework\Messages::throwException('CREATE_FAILURE', ['what' => 'Entity "Person"']);
		$data['requests']['lead']['payload']['person_id'] = $data['requests']['person']['response']['body']['data']['id'];
		return $data;
	}

	// ####################################################################################################
	public static function makeApiRequestLead(array $data = []): array {
		$requestData = array_merge($data['requests']['lead'], ['domain' => $data['domain'], 'token' => $data['token']]);
		$data['requests']['lead']['response'] = static::makeApiRequest($requestData);
		// Checking HTTP code
		if(!in_array($data['requests']['lead']['response']['status'], [200, 201])) \Rou9e\Framework\Messages::throwException('CREATE_FAILURE', ['what' => 'Entity "Lead"']);
		// Checking 'success' field
		if($data['requests']['lead']['response']['body']['success'] !== true) \Rou9e\Framework\Messages::throwException('CREATE_FAILURE', ['what' => 'Entity "Lead"']);
		$data['requests']['note']['payload']['lead_id'] = $data['requests']['lead']['response']['body']['data']['id'];
		return $data;
	}

	// ####################################################################################################
	public static function makeApiRequestNote(array $data = []): array {
		$requestData = array_merge($data['requests']['note'], ['domain' => $data['domain'], 'token' => $data['token']]);
		$data['requests']['note']['response'] = static::makeApiRequest($requestData);
		// Checking HTTP code
		if(!in_array($data['requests']['note']['response']['status'], [200, 201])) \Rou9e\Framework\Messages::throwException('CREATE_FAILURE', ['what' => 'Entity "Note"']);
		// Checking 'success' field
		if($data['requests']['note']['response']['body']['success'] !== true) \Rou9e\Framework\Messages::throwException('CREATE_FAILURE', ['what' => 'Entity "Note"']);
		return $data;
	}

	// ####################################################################################################
	public static function handleException(\Exception $e, array $data = []) {
		unset($data['token']);
		// Logging the exception
		\Rou9e\Wordpress\Core::log('#################################################################################################### PIPEDRIVE FAILURE');
		\Rou9e\Wordpress\Core::log($e->getMessage());
		\Rou9e\Wordpress\Core::log($data);
		// Sending emergency email
		$urlData = \Rou9e\Framework\Core::parseUrl('', 'host');
		$recipients = $data['emails'];
		$emailTitle = $urlData['host']." - Pipedrive connection problem (".$data['formdata']['email'].")";
		$emailContent = "There was an issue about this lead : ".$data['formdata']['email']." (email may already exist in Pipedrive)";
		$email = wp_mail($recipients, $emailTitle, $emailContent);
		if(static::DEBUG) { echo json_encode(['data' => $data, 'exception' => $e->getMessage()]); exit; }
	}

	// ####################################################################################################
	public static function pushFormDataToPipedrive(array $formData = []) {
		try {
			// Initiating
			$data = static::getRequestData($formData);
			// Creating new Person
			$data = static::makeApiRequestPerson($data);
			// Creating new Lead
			$data = static::makeApiRequestLead($data);
			// Creating new Note
			$data = static::makeApiRequestNote($data);
			// Handling response
			unset($data['token']);
			if(static::DEBUG) {
				echo json_encode($data);
				\Rou9e\Wordpress\Core::log('#################################################################################################### PIPEDRIVE SUCCESS');
				\Rou9e\Wordpress\Core::log($data);
			}
			return true;
		}
		catch(\Exception $e) {
			if(!isset($data)) $data = $formData;
			static::handleException($e, $data);
			return false;
		}
	}
}