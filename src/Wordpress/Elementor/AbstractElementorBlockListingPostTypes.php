<?php

namespace Rou9e\Wordpress\Elementor;

if(!defined('ABSPATH')) { exit; }

abstract class AbstractElementorBlockListingPostTypes extends \Rou9e\Wordpress\Elementor\AbstractElementorBlock {

	public function get_keywords() {
		return ['post_type', 'posttype', 'query', 'list', 'single'];
	}

	public function get_icon() {
		return 'eicon-loop-builder';
	}

	protected function register_controls() {
		// General
		$this->start_controls_section('general_section', [
			'label' => esc_html__('General', 'elementor'),
			'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
		]);
		$this->registerControlsSectionGeneral();
		$this->end_controls_section();
		// Content
		$this->start_controls_section('content_section', [
			'label' => esc_html__('Content', 'elementor'),
			'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
		]);
		$this->registerControlsSectionContent();
		$this->end_controls_section();
		// Query
		$this->start_controls_section('query_section', [
			'label' => esc_html__('Query', 'elementor'),
			'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
		]);
		$this->registerControlsSectionQuery();
		$this->end_controls_section();
		// Settings
		$this->start_controls_section('settings_section', [
			'label' => esc_html__('Settings', 'elementor'),
			'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
		]);
		$this->registerControlsSectionSettings();
		$this->end_controls_section();
	}

	public function registerControlsSectionGeneral() {
		$this->add_control('title', [
			'label' => esc_html__('Title', 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
			'placeholder' => esc_html__('Some title', 'elementor'),
		]);
		$this->add_control('baseline', [
			'label' => esc_html__('Baseline', 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
			'placeholder' => esc_html__('Some baseline', 'elementor'),
		]);
		$this->add_control('image', [
			'label' => esc_html__('Image', 'elementor'),
			'type' => \Elementor\Controls_Manager::MEDIA,
			'default' => [
				'url' => '',
			],
		]);
	}

	public function registerControlsSectionContent() {
		$this->add_control('before_results', [
			'label' => esc_html__('Before results', 'elementor'),
			'type' => \Elementor\Controls_Manager::WYSIWYG,
			'placeholder' => esc_html__('Some content', 'elementor'),
		]);
		$this->add_control('after_results', [
			'label' => esc_html__('After results', 'elementor'),
			'type' => \Elementor\Controls_Manager::WYSIWYG,
			'placeholder' => esc_html__('Some content', 'elementor'),
		]);
		$this->add_control('no_result', [
			'label' => esc_html__('No result', 'elementor'),
			'type' => \Elementor\Controls_Manager::WYSIWYG,
			'placeholder' => esc_html__('There is no result', 'elementor'),
		]);
	}

	public function registerControlsSectionQuery() {
		$this->add_control('post_type',[
			'label' => esc_html__('Source', 'elementor'),
			'type' => \Elementor\Controls_Manager::SELECT,
			'default' => $this->getDefaulForPostTypesList(),
			'options' => $this->getAllowedPostTypesList(),
		]);
		$this->add_control('order_by',[
			'label' => esc_html__('Order by', 'elementor'),
			'type' => \Elementor\Controls_Manager::SELECT,
			'default' => 'title',
			'options' => $this->getOrderByOptionsBy(),
		]);
		$this->add_control('order',[
			'label' => esc_html__('Order', 'elementor'),
			'type' => \Elementor\Controls_Manager::SELECT,
			'default' => 'ASC',
			'options' => $this->getOrderByOptions(),
		]);
		$this->add_control('max_items', [
			'label' => esc_html__('Max items', 'elementor'),
			'type' => \Elementor\Controls_Manager::NUMBER,
			'min' => 0,
			'max' => 999,
			'default' => 0,
		]);
		$this->add_control('filter_guids', [
			'label' => esc_html__('Filter GUIDs', 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXTAREA,
			'placeholder' => esc_html__('List selected GUIDs (comma separated)', 'elementor'),
		]);
		$this->add_control('exclude_guids', [
			'label' => esc_html__('Exclude GUIDs', 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXTAREA,
			'placeholder' => esc_html__('List excluded GUIDs (comma separated)', 'elementor'),
		]);
	}

	public function registerControlsSectionSettings() {
		$this->add_control('css_class', [
			'label' => esc_html__('CSS class', 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
		]);
	}

	public function getAllowedPostTypesList() {
		return \Rou9e\Wordpress\Core::getCleanedPostTypesList();
	}

	public function getDefaulForPostTypesList() {
		return 'post';
	}

	public function getOrderByOptionsBy() {
		return [
			'title' => esc_html__('title', 'elementor'),
			'publish_date' => esc_html__('published_at', 'elementor'),
			'modified' => esc_html__('updated_at', 'elementor'),
			'rand' => esc_html__('random', 'elementor'),
		];
	}

	public function getOrderByOptions() {
		return [
			'ASC' => esc_html__('ASC', 'elementor'),
			'DESC' => esc_html__('DESC', 'elementor'),
		];
	}

	public function getBlockConfiguration(): array {
		// Getting back data
		$settings = $this->getSettingsForDisplay();
		$data['title'] = @$settings['title'] ?? '';
		$data['baseline'] = @$settings['baseline'] ?? '';
		$data['image'] = @$settings['image']['url'] ?? '';
		$data['before_results'] = @$settings['before_results'] ?? '';
		$data['after_results'] = @$settings['after_results'] ?? '';
		$data['no_result'] = @$settings['no_result'] ?? '';
		$data['post_type'] = @$settings['post_type'] ?? '';
		$data['order_by'] = @$settings['order_by'] ?? 'title';
		$data['order'] = @$settings['order'] ?? 'ASC';
		$data['max_items'] = @$settings['max_items'] ?? '-1';
		$data['filter_guids'] = @$settings['filter_guids'] ?? '';
		$data['exclude_guids'] = @$settings['exclude_guids'] ?? '';
		$data['css_class'] = @$settings['css_class'] ?? '';
		// Parsing data
		$data = array_map('trim', $data);
		$data['max_items'] = intval($data['max_items']);
		$data['filter_guids'] = \Rou9e\Framework\Core::cleanList($data['filter_guids'], [',']);
		$data['exclude_guids'] = \Rou9e\Framework\Core::cleanList($data['exclude_guids'], [',']);
		// Returing data
		return $data;
	}

	public function getQueryOptions(string $postType = '', string $orderBy = 'title', string $order = 'ASC', int $max = -1, array $inGuids = [], array $outGuids = []): array {
		if($max === 0) $max = -1;
		$options = [
			'post_type'						=> $postType,
			'post_status'					=> 'publish',
			'posts_per_page'			=> -1,
			'max'									=> $max,								// instead of 'posts_per_page' : manually filtered
			'orderby'							=> $orderBy,
			'order'								=> $order,
		];
		if($options['orderby'] === 'priority') {
			$options['orderby'] = 'meta_value';
			$options['meta_key'] = 'priority';
		}
		$options['meta_query'] = ['relation' => 'AND'];
		if(count($inGuids) > 0) {
			$options['meta_query'][] = [
				'key'							=> 'guid',
				'compare'					=> 'IN',
				'value'						=> $inGuids,
			];
		}
		if(count($outGuids) > 0) {
			$options['meta_query'][] = [
				'key'							=> 'guid',
				'compare'					=> 'NOT IN',
				'value'						=> $outGuids,
			];
		}
		return $options;
	}

	public function getQueryResults(string $postType = '', string $orderBy = 'title', string $order = 'ASC', int $max = -1, array $inGuids = [], array $outGuids = []): array {
		if($postType === '') return [];
		$options = $this->getQueryOptions($postType, $orderBy, $order, $max, $inGuids, $outGuids);
		$query = new \WP_Query($options);
		$items = $query->posts;
		wp_reset_postdata();
		return $items;
	}

	public function getQueryResultsEnhanced(array $items = [], int $max = -1): array {
		//foreach($items as & $item) {
		//	$item->acf = [];
		//	$item->acf['guid'] = \Rou9e\Wordpress\Acf::getAcfValue('guid', '', $item->ID);
		//	$item->acf['title'] = $item->post_title;
		//	$item->acf['image'] = \Rou9e\Wordpress\Acf::getAcfImageValue('image', '', $item->ID);
		//	if($item->acf['image'] === '') $item->acf['image'] = \Rou9e\Wordpress\Core::getThemeUrl().'pub/img/client-placeholder.jpg';
		//	$item->acf['image_label'] = \Rou9e\Wordpress\Acf::getAcfValue('image_label', '', $item->ID);
		//	if($item->acf['image_label'] === '') $item->acf['image_label'] = $item->acf['title'];
		//	$item->acf['url'] = \Rou9e\Wordpress\Acf::getAcfLinkValue('url', '', $item->ID);
		//}
		$max = intval($max);
		if($max > 0) $items = array_slice($items, 0, $max);
		return $items;
	}

	protected function render() {
		$items = [];
		$data = $this->getBlockConfiguration();
		if($data['post_type'] !== '') $items = $this->getQueryResults($data['post_type'], $data['order_by'], $data['order'], $data['max_items'], $data['filter_guids'], $data['exclude_guids']);
		$items = $this->getQueryResultsEnhanced($items, $data['max_items']);
		$html = '';
		$html .= '<div class="elementor-listing elementor-listing-'.$data['post_type'].' '.$data['css_class'].'">';
		if($data['title'] !== '') $html .= '<h1 class="elementor-listing-title">'.$data['title'].'</h1>';
		if($data['baseline'] !== '') $html .= '<h2 class="elementor-listing-baseline">'.$data['baseline'].'</h2>';
		if($data['image'] !== '') $html .= '<div class="elementor-listing-image"><img class="img-fluid" src="'.$data['image'].'" title="'.$data['title'].'" alt="'.$data['title'].'" /></div>';
		if($data['before_results'] !== '') $html .= '<div class="elementor-listing-before">'.$data['before_results'].'</div>';		if($data['post_type'] === '' || count($items) === 0) {
			$html .= '<div class="elementor-listing-empty">'.$data['no_result'].'</div>';
		}
		else {
			$html .= '<div class="elementor-listing-items">';
			foreach($items as $item) {
				$html .= '	<div class="elementor-listing-item" data-id="'.$item->ID.'">';
				$html .= '		<a class="elementor-listing-item-link" href="'.$item->acf['url'].'">';
				$html .= '			<div class="elementor-listing-item-image">';
				$html .= '				<img class="img-fluid" src="'.$item->acf['image'].'" title="'.$item->acf['image_label'].'" alt="'.$item->acf['image_label'].'"/>';
				$html .= '			</div>';
				$html .= '			<div class="elementor-listing-item-title">'.$item->acf['title'].'</div>';
				$html .= '		</a>';
				$html .= '	</div>';
			}
			$html .= '</div>';
		}
		if($data['after_results'] !== '') $html .= '<div class="elementor-listing-after">'.$data['after_results'].'</div>';
		$html .= '</div>';
		echo $html;
	}
}
