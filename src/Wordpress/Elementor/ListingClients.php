<?php

namespace Rou9e\Wordpress\Elementor;

// To add in functions.php :
// 	add_action('elementor/widgets/register', function($widgets_manager) {
// 		$widgets_manager->register(new \Rou9e\Wordpress\Elementor\ListingClients());
// 	});

class ListingClients extends \Rou9e\Wordpress\Elementor\AbstractElementorBlockListingPostTypes {

	public function getBlockName(): string {
		return 'elementor-listing-clients';
	}

	public function getBlockTitle(): string {
		return 'elementor-listing-clients';
	}

	public function get_keywords() {
		$keywords = parent::get_keywords();
		$keywords[] = 'client';
		$keywords[] = 'clients';
		return $keywords;
	}

	public function getAllowedPostTypesList() {
		return ['client' => 'client'];
	}

	public function getDefaulForPostTypesList() {
		return 'client';
	}

	public function getAvailableSectors() {
		$query = new \WP_Query([
			'post_type'						=> 'sector',
			'post_status'					=> 'publish',
			'posts_per_page'			=> -1,
			'orderby'							=> 'title',
			'order'								=> 'ASC',
		]);
		$rows = $query->posts;
		wp_reset_postdata();
		$items = [];
		foreach($rows as $row) {
			$items[\Rou9e\Wordpress\Acf::getAcfValue('guid', '', $row->ID)] = $row->post_title;
		}
		return $items;
	}

	public function getAvailableSkills() {
		$query = new \WP_Query([
			'post_type'						=> 'skill',
			'post_status'					=> 'publish',
			'posts_per_page'			=> -1,
			'orderby'							=> 'title',
			'order'								=> 'ASC',
		]);
		$rows = $query->posts;
		wp_reset_postdata();
		$items = [];
		foreach($rows as $row) {
			$items[\Rou9e\Wordpress\Acf::getAcfValue('guid', '', $row->ID)] = $row->post_title;
		}
		return $items;
	}

	public function registerControlsSectionQuery() {
		parent::registerControlsSectionQuery();
		$this->add_control('sectors', [
			'label' => esc_html__('Filtering by sectors', 'elementor'),
			'type' => \Elementor\Controls_Manager::SELECT2,
			'label_block' => true,
			'multiple' => true,
			'default' => [],
			'options' => $this->getAvailableSectors(),
		]);
		$this->add_control('skills', [
			'label' => esc_html__('Filtering by skills', 'elementor'),
			'type' => \Elementor\Controls_Manager::SELECT2,
			'label_block' => true,
			'multiple' => true,
			'default' => [],
			'options' => $this->getAvailableSkills(),
		]);
	}

	public function getOrderByOptionsBy() {
		$options = parent::getOrderByOptionsBy();
		$options['priority'] = esc_html__('priority', 'elementor');
		return $options;
	}

	public function getBlockConfiguration(): array {
		$data = parent::getBlockConfiguration();
		$settings = $this->getSettingsForDisplay();
		$data['sectors'] = @$settings['sectors'] ?? [];
		$data['skills'] = @$settings['skills'] ?? [];
		return $data;
	}

	public function getQueryResultsEnhanced(array $items = [], int $max = -1): array {
		$enhancedItems = [];
		$data = $this->getBlockConfiguration();
		foreach($items as & $item) {
			$item->acf = [];
			$item->acf['guid'] = \Rou9e\Wordpress\Acf::getAcfValue('guid', '', $item->ID);
			$item->acf['title'] = $item->post_title;
			$item->acf['image'] = \Rou9e\Wordpress\Acf::getAcfImageValue('image', '', $item->ID);
			if($item->acf['image'] === '') $item->acf['image'] = \Rou9e\Wordpress\Core::getThemeUrl().'pub/img/skill-placeholder.jpg';
			$item->acf['image_label'] = \Rou9e\Wordpress\Acf::getAcfValue('image_label', '', $item->ID);
			if($item->acf['image_label'] === '') $item->acf['image_label'] = $item->acf['title'];
			//$item->acf['url'] = \Rou9e\Wordpress\Acf::getAcfLinkValue('url', '', $item->ID);
			$item->acf['priority'] = intval(\Rou9e\Wordpress\Acf::getAcfValue('priority', 0, $item->ID));
			// ########## Sectors
			$item->acf['sectors_ids'] = \Rou9e\Wordpress\Acf::getAcfValue('sectors', [], $item->ID);
			if(in_array($item->acf['sectors_ids'], ['', null, false])) $item->acf['sectors_ids'] = [];
			if(!is_array($item->acf['sectors_ids'])) $item->acf['sectors_ids'] = [$item->acf['sectors_ids']];
			$item->acf['sectors_guids'] = [];
			$item->acf['sectors'] = [];
			foreach($item->acf['sectors_ids'] as $id) {
				$sector = get_post($id);
				$sector->acf = [];
				$sector->acf['guid'] = \Rou9e\Wordpress\Acf::getAcfValue('guid', '', $id);
				$item->acf['sectors'][] = $sector;
			}
			foreach($item->acf['sectors'] as $sector) $item->acf['sectors_guids'][] = $sector->acf['guid'];
			// ########## Skills
			$item->acf['skills_ids'] = \Rou9e\Wordpress\Acf::getAcfValue('skills', [], $item->ID);
			if(in_array($item->acf['skills_ids'], ['', null, false])) $item->acf['skills_ids'] = [];
			if(!is_array($item->acf['skills_ids'])) $item->acf['skills_ids'] = [$item->acf['skills_ids']];
			$item->acf['skills_guids'] = [];
			$item->acf['skills'] = [];
			foreach($item->acf['skills_ids'] as $id) {
				$skill = get_post($id);
				$skill->acf = [];
				$skill->acf['guid'] = \Rou9e\Wordpress\Acf::getAcfValue('guid', '', $id);
				$item->acf['skills'][] = $skill;
			}
			foreach($item->acf['skills'] as $skill) $item->acf['skills_guids'][] = $skill->acf['guid'];
			// ########## Filtering
			if(count($data['sectors']) > 0 || count($data['skills']) > 0) {
				if(count($data['sectors']) > 0) {
					if(count(array_intersect($data['sectors'], $item->acf['sectors_guids'])) > 0) {
						$enhancedItems[$item->acf['guid']] = $item;
					}
				}
				if(count($data['skills']) > 0) {
					if(count(array_intersect($data['skills'], $item->acf['skills_guids'])) > 0) {
						$enhancedItems[$item->acf['guid']] = $item;
					}
				}
			}
			else {
				$enhancedItems[$item->acf['guid']] = $item;
			}
		}
		$enhancedItems = parent::getQueryResultsEnhanced($enhancedItems, $max);
		return $enhancedItems;
	}

	protected function render() {
		$items = [];
		$data = $this->getBlockConfiguration();
		if($data['post_type'] !== '') $items = $this->getQueryResults($data['post_type'], $data['order_by'], $data['order'], $data['max_items'], $data['filter_guids'], $data['exclude_guids']);
		$items = $this->getQueryResultsEnhanced($items, $data['max_items']);
		$html = '';
		$html .= '<div class="elementor-listing elementor-listing-'.$data['post_type'].' '.$data['css_class'].'">';
		if($data['title'] !== '') $html .= '<h1 class="elementor-listing-title">'.$data['title'].'</h1>';
		if($data['baseline'] !== '') $html .= '<h2 class="elementor-listing-baseline">'.$data['baseline'].'</h2>';
		if($data['image'] !== '') $html .= '<div class="elementor-listing-image"><img class="img-fluid" src="'.$data['image'].'" title="'.$data['title'].'" alt="'.$data['title'].'" /></div>';
		if($data['before_results'] !== '') $html .= '<div class="elementor-listing-before">'.$data['before_results'].'</div>';
		if($data['post_type'] === '' || count($items) === 0) {
			$html .= '<div class="elementor-listing-empty">'.$data['no_result'].'</div>';
		}
		else {
			$html .= '<div class="elementor-listing-items">';
			foreach($items as $item) {
				$html .= '	<div class="elementor-listing-item" data-id="'.$item->ID.'">';
				//$html .= '		<a class="elementor-listing-item-link" href="'.$item->acf['url'].'">';
				$html .= '			<div class="elementor-listing-item-image">';
				$html .= '				<img class="img-fluid" src="'.$item->acf['image'].'" title="'.$item->acf['image_label'].'" alt="'.$item->acf['image_label'].'"/>';
				$html .= '			</div>';
				//$html .= '			<div class="elementor-listing-item-title">'.$item->acf['title'].'</div>';
				//$html .= '		</a>';
				$html .= '	</div>';
			}
			$html .= '</div>';
		}
		if($data['after_results'] !== '') $html .= '<div class="elementor-listing-after">'.$data['after_results'].'</div>';
		$html .= '</div>';
		echo $html;
	}
}
