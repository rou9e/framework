<?php

namespace Rou9e\Wordpress\Elementor;

// To add in functions.php :
// 	add_action('elementor/widgets/register', function($widgets_manager) {
// 		$widgets_manager->register(new \Rou9e\Wordpress\Elementor\ListingPosts());
// 	});

class ListingPosts extends \Rou9e\Wordpress\Elementor\AbstractElementorBlockListingPostTypes {

	public function getBlockName(): string {
		return 'elementor-listing-posts';
	}

	public function getBlockTitle(): string {
		return 'elementor-listing-posts';
	}

	public function get_keywords() {
		$keywords = parent::get_keywords();
		$keywords[] = 'post';
		$keywords[] = 'posts';
		return $keywords;
	}

	public function getAllowedPostTypesList() {
		return ['post' => 'post'];
	}

	public function getDefaulForPostTypesList() {
		return 'post';
	}

	public function getAvailablePostCategories() {
		$items = \Rou9e\Wordpress\Core::getPostsCategories();
		$categories = [];
		foreach($items as $item) {
			$categories[$item->cat_ID] = $item->name;
		}
		return $categories;
	}

	public function registerControlsSectionQuery() {
		parent::registerControlsSectionQuery();
		$this->add_control('categories', [
			'label' => esc_html__('Filtering by categories', 'elementor'),
			'type' => \Elementor\Controls_Manager::SELECT2,
			'label_block' => true,
			'multiple' => true,
			'default' => [],
			'options' => $this->getAvailablePostCategories(),
		]);
	}

	public function getBlockConfiguration(): array {
		$data = parent::getBlockConfiguration();
		$settings = $this->getSettingsForDisplay();
		$data['categories'] = @$settings['categories'] ?? [];
		return $data;
	}

	public function getQueryOptions(string $postType = '', string $orderBy = 'title', string $order = 'ASC', int $max = -1, array $inGuids = [], array $outGuids = []): array {
		$options = parent::getQueryOptions($postType, $orderBy, $order, $max, $inGuids, $outGuids);
		$settings = $this->getSettingsForDisplay();
		if(count($settings['categories']) > 0) {
			$options['category__in'] = $settings['categories'];
		}
		return $options;
	}

	protected function render() {
		$items = [];
		$data = $this->getBlockConfiguration();
		if($data['post_type'] !== '') $items = $this->getQueryResults($data['post_type'], $data['order_by'], $data['order'], $data['max_items'], $data['filter_guids'], $data['exclude_guids']);
		$items = $this->getQueryResultsEnhanced($items, $data['max_items']);
		$html = '';
		$html .= '<div class="elementor-listing elementor-listing-'.$data['post_type'].' '.$data['css_class'].'">';
		if($data['title'] !== '') $html .= '<h1 class="elementor-listing-title">'.$data['title'].'</h1>';
		if($data['baseline'] !== '') $html .= '<h2 class="elementor-listing-baseline">'.$data['baseline'].'</h2>';
		if($data['image'] !== '') $html .= '<div class="elementor-listing-image"><img class="img-fluid" src="'.$data['image'].'" title="'.$data['title'].'" alt="'.$data['title'].'" /></div>';
		if($data['before_results'] !== '') $html .= '<div class="elementor-listing-before">'.$data['before_results'].'</div>';
		if($data['post_type'] === '' || count($items) === 0) {
			$html .= '<div class="elementor-listing-empty">'.$data['no_result'].'</div>';
		}
		else {
			$html .= '<div class="elementor-listing-items">';
			foreach($items as $item) {
				$html .= '	<div class="elementor-listing-item" data-id="'.$item->ID.'">';
				$html .= '		<a class="elementor-listing-item-link" href="'.get_post_permalink($item->ID).'">';
				$html .= '			<div class="elementor-listing-item-image">';
				$html .= '				<img class="img-fluid" src="'.get_the_post_thumbnail_url($item->ID).'" title="'.$item->post_title.'" alt="'.$item->post_title.'"/>';
				$html .= '			</div>';
				$html .= '			<div class="elementor-listing-item-title">'.$item->post_title.'</div>';
				$html .= '		</a>';
				$html .= '	</div>';
			}
			$html .= '</div>';
		}
		if($data['after_results'] !== '') $html .= '<div class="elementor-listing-after">'.$data['after_results'].'</div>';
		$html .= '</div>';
		echo $html;
	}
}
