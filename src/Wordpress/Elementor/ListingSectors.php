<?php

namespace Rou9e\Wordpress\Elementor;

// To add in functions.php :
// 	add_action('elementor/widgets/register', function($widgets_manager) {
// 		$widgets_manager->register(new \Rou9e\Wordpress\Elementor\ListingSectors());
// 	});

class ListingSectors extends \Rou9e\Wordpress\Elementor\AbstractElementorBlockListingPostTypes {

	public function getBlockName(): string {
		return 'elementor-listing-sectors';
	}

	public function getBlockTitle(): string {
		return 'elementor-listing-sectors';
	}

	public function get_keywords() {
		$keywords = parent::get_keywords();
		$keywords[] = 'sector';
		$keywords[] = 'sectors';
		return $keywords;
	}

	public function getAllowedPostTypesList() {
		return ['sector' => 'sector'];
	}

	public function getDefaulForPostTypesList() {
		return 'sector';
	}

	public function getQueryResultsEnhanced(array $items = [], int $max = -1): array {
		foreach($items as & $item) {
			$item->acf = [];
			$item->acf['guid'] = \Rou9e\Wordpress\Acf::getAcfValue('guid', '', $item->ID);
			$item->acf['title'] = $item->post_title;
			$item->acf['image'] = \Rou9e\Wordpress\Acf::getAcfImageValue('image', '', $item->ID);
			if($item->acf['image'] === '') $item->acf['image'] = \Rou9e\Wordpress\Core::getThemeUrl().'pub/img/sector-placeholder.jpg';
			$item->acf['image_label'] = \Rou9e\Wordpress\Acf::getAcfValue('image_label', '', $item->ID);
			if($item->acf['image_label'] === '') $item->acf['image_label'] = $item->acf['title'];
			$item->acf['url'] = \Rou9e\Wordpress\Acf::getAcfLinkValue('url', '', $item->ID);
		}
		$items = parent::getQueryResultsEnhanced($items, $max);
		return $items;
	}

	protected function render() {
		$items = [];
		$data = $this->getBlockConfiguration();
		if($data['post_type'] !== '') $items = $this->getQueryResults($data['post_type'], $data['order_by'], $data['order'], $data['max_items'], $data['filter_guids'], $data['exclude_guids']);
		$items = $this->getQueryResultsEnhanced($items);
		$html = '';
		$html .= '<div class="elementor-listing elementor-listing-'.$data['post_type'].' '.$data['css_class'].'">';
		if($data['title'] !== '') $html .= '<h1 class="elementor-listing-title">'.$data['title'].'</h1>';
		if($data['baseline'] !== '') $html .= '<h2 class="elementor-listing-baseline">'.$data['baseline'].'</h2>';
		if($data['image'] !== '') $html .= '<div class="elementor-listing-image"><img class="img-fluid" src="'.$data['image'].'" title="'.$data['title'].'" alt="'.$data['title'].'" /></div>';
		if($data['before_results'] !== '') $html .= '<div class="elementor-listing-before">'.$data['before_results'].'</div>';
		if($data['post_type'] === '' || count($items) === 0) {
			$html .= '<div class="elementor-listing-empty">'.$data['no_result'].'</div>';
		}
		else {
			$html .= '<script type="text/javascript">';
			$html .= '	jQuery(document).ready(function() {';
			$html .= '		let displaySectors = false;';
			$html .= '		if(localStorage.getItem("alioze.sector_visited") !== "1") displaySectors = true;';
			$html .= '		if(jQuery("body").hasClass("admin-bar")) displaySectors = true;';
			$html .= '		if(window.location.href.includes("/wp-admin/")) displaySectors = true;';
			$html .= '		if(window.location.href.includes("elementor-preview=")) displaySectors = true;';
			$html .= '		if(displaySectors === true) {';
			$html .= '			jQuery(".elementor-listing-sector .elementor-listing-items").css({display: "flex"});';
			$html .= '		}';
			$html .= '	})';
			$html .= '</script>';
			$html .= '<div class="elementor-listing-items">';
			foreach($items as $item) {
				$html .= '	<div class="elementor-listing-item" data-id="'.$item->ID.'">';
				$html .= '		<a class="elementor-listing-item-link" href="'.$item->acf['url'].'">';
				$html .= '			<div class="elementor-listing-item-image">';
				$html .= '				<img class="img-fluid" src="'.$item->acf['image'].'" title="'.$item->acf['image_label'].'" alt="'.$item->acf['image_label'].'"/>';
				$html .= '			</div>';
				$html .= '			<div class="elementor-listing-item-title">'.$item->acf['title'].'</div>';
				$html .= '		</a>';
				$html .= '	</div>';
			}
			$html .= '</div>';
		}
		if($data['after_results'] !== '') $html .= '<div class="elementor-listing-after">'.$data['after_results'].'</div>';
		$html .= '</div>';
		echo $html;
	}
}
