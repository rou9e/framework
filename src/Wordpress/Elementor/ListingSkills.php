<?php

namespace Rou9e\Wordpress\Elementor;

// To add in functions.php :
// 	add_action('elementor/widgets/register', function($widgets_manager) {
// 		$widgets_manager->register(new \Rou9e\Wordpress\Elementor\ListingSkills());
// 	});

class ListingSkills extends \Rou9e\Wordpress\Elementor\AbstractElementorBlockListingPostTypes {

	public function getBlockName(): string {
		return 'elementor-listing-skills';
	}

	public function getBlockTitle(): string {
		return 'elementor-listing-skills';
	}

	public function get_keywords() {
		$keywords = parent::get_keywords();
		$keywords[] = 'skill';
		$keywords[] = 'skills';
		return $keywords;
	}

	public function getAllowedPostTypesList() {
		return ['skill' => 'skill'];
	}

	public function getDefaulForPostTypesList() {
		return 'skill';
	}

	public function getAvailableSectors() {
		$query = new \WP_Query([
			'post_type'						=> 'sector',
			'post_status'					=> 'publish',
			'posts_per_page'			=> -1,
			'orderby'							=> 'title',
			'order'								=> 'ASC',
		]);
		$rows = $query->posts;
		wp_reset_postdata();
		$items = [];
		foreach($rows as $row) {
			$items[\Rou9e\Wordpress\Acf::getAcfValue('guid', '', $row->ID)] = $row->post_title;
		}
		return $items;
	}

	public function registerControlsSectionQuery() {
		parent::registerControlsSectionQuery();
		$this->add_control('sectors', [
			'label' => esc_html__('Filtering by sectors', 'elementor'),
			'type' => \Elementor\Controls_Manager::SELECT2,
			'label_block' => true,
			'multiple' => true,
			'default' => [],
			'options' => $this->getAvailableSectors(),
		]);
	}

	public function getBlockConfiguration(): array {
		$data = parent::getBlockConfiguration();
		$settings = $this->getSettingsForDisplay();
		$data['sectors'] = @$settings['sectors'] ?? [];
		return $data;
	}

	public function getQueryResultsEnhanced(array $items = [], int $max = -1): array {
		$enhancedItems = [];
		$data = $this->getBlockConfiguration();
		foreach($items as & $item) {
			$item->acf = [];
			$item->acf['guid'] = \Rou9e\Wordpress\Acf::getAcfValue('guid', '', $item->ID);
			$item->acf['title'] = $item->post_title;
			$item->acf['url'] = \Rou9e\Wordpress\Acf::getAcfLinkValue('url', '', $item->ID);
			// ########## Sectors
			$item->acf['sectors_ids'] = \Rou9e\Wordpress\Acf::getAcfValue('sectors', [], $item->ID);
			$item->acf['sectors_guids'] = [];
			$item->acf['sectors'] = [];
			foreach($item->acf['sectors_ids'] as $id) {
				$sector = get_post($id);
				$sector->acf = [];
				$sector->acf['guid'] = \Rou9e\Wordpress\Acf::getAcfValue('guid', '', $id);
				$item->acf['sectors'][] = $sector;
			}
			foreach($item->acf['sectors'] as $sector) $item->acf['sectors_guids'][] = $sector->acf['guid'];
			// ########## Filtering
			if(count($data['sectors']) > 0) {
				if(count(array_intersect($data['sectors'], $item->acf['sectors_guids'])) > 0) {
					$enhancedItems[$item->acf['guid']] = $item;
				}
			}
			else {
				$enhancedItems[$item->acf['guid']] = $item;
			}
		}
		$enhancedItems = parent::getQueryResultsEnhanced($enhancedItems, $max);
		return $enhancedItems;
	}

	protected function render() {
		$items = [];
		$data = $this->getBlockConfiguration();
		if($data['post_type'] !== '') $items = $this->getQueryResults($data['post_type'], $data['order_by'], $data['order'], $data['max_items'], $data['filter_guids'], $data['exclude_guids']);
		$items = $this->getQueryResultsEnhanced($items, $data['max_items']);
		$html = '';
		$html .= '<div class="elementor-listing elementor-listing-'.$data['post_type'].' '.$data['css_class'].'">';
		if($data['title'] !== '') $html .= '<h1 class="elementor-listing-title">'.$data['title'].'</h1>';
		if($data['baseline'] !== '') $html .= '<h2 class="elementor-listing-baseline">'.$data['baseline'].'</h2>';
		if($data['image'] !== '') $html .= '<div class="elementor-listing-image"><img class="img-fluid" src="'.$data['image'].'" title="'.$data['title'].'" alt="'.$data['title'].'" /></div>';
		if($data['before_results'] !== '') $html .= '<div class="elementor-listing-before">'.$data['before_results'].'</div>';		if($data['post_type'] === '' || count($items) === 0) {
			$html .= '<div class="elementor-listing-empty">'.$data['no_result'].'</div>';
		}
		else {
			$html .= '<div class="elementor-listing-items">';
			$svg = '<svg aria-hidden="true" class="e-font-icon-svg e-fas-plus" viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"><path d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>';
			foreach($items as $item) {
				$html .= '	<div class="elementor-listing-item" data-id="'.$item->ID.'">';
				$html .= '		<a class="elementor-icon-list-item" href="'.$item->acf['url'].'">';
				$html .= '			<span class="elementor-icon-list-icon">'.$svg.'</span>';
				$html .= '			<span class="elementor-icon-list-text">'.$item->acf['title'].'</span>';
				$html .= '		</a>';
				$html .= '	</div>';
			}
			$html .= '</div>';
		}
		if($data['after_results'] !== '') $html .= '<div class="elementor-listing-after">'.$data['after_results'].'</div>';
		$html .= '</div>';
		echo $html;
	}
}
