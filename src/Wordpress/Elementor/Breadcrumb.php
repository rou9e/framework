<?php

namespace Rou9e\Wordpress\Elementor;

// To add in functions.php :
// 	add_action('elementor/widgets/register', function($widgets_manager) {
// 		$widgets_manager->register(new \Rou9e\Wordpress\Elementor\Breadcrumb());
// 	});

class Breadcrumb extends \Rou9e\Wordpress\Elementor\AbstractElementorBlock {

	public string $homeLabel = 'Home';
	public string $blogLabel = 'Blog';
	public string $lastItemLabel = '';
	public string $lastItemHasLink = 'false';
	public string $generateRichsnippets = 'true';
	public string $separator = '>';
	public string $cssClass = '';

	public function getBlockName(): string {
		return 'elementor-breadcrumb';
	}

	public function getBlockTitle(): string {
		return 'elementor-breadcrumb';
	}

	public function get_keywords() {
		$keywords = [];
		$keywords[] = 'ariane';
		$keywords[] = 'breadcrumb';
		$keywords[] = 'breadcrumbs';
		return $keywords;
	}

	public function get_icon() {
		return 'eicon-chevron-double-right';
	}

	protected function register_controls() {
		// General
		$this->start_controls_section('general_section', [
			'label' => esc_html__('General', 'elementor'),
			'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
		]);
		$this->registerControlsSectionGeneral();
		$this->end_controls_section();
	}

	public function registerControlsSectionGeneral() {
		$this->add_control('home_label', [
			'label' => esc_html__("Home label", 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
			'default' => __("Home", 'elementor'),
		]);
		$this->add_control('blog_label', [
			'label' => esc_html__("Blog label", 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
			'default' => __("Blog", 'elementor'),
		]);
		$this->add_control('last_item_label', [
			'label' => esc_html__("Last item title", 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
			'placeholder' => esc_html__("", 'elementor'),
		]);
		$this->add_control('last_item_has_link', [
			'label' => esc_html__("Last item has link", 'elementor'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'label_on' => esc_html__('Yes', 'elementor'),
			'label_off' => esc_html__('No', 'elementor'),
			'return_value' => "true",
			'default' => "false",
		]);
		$this->add_control('generate_richsnippets', [
			'label' => esc_html__("Generate RichSnippets ?", 'elementor'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'label_on' => esc_html__('Yes', 'elementor'),
			'label_off' => esc_html__('No', 'elementor'),
			'return_value' => "true",
			'default' => "true",
		]);
		$this->add_control('separator', [
			'label' => esc_html__("Separator", 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
			'default' => ">",
		]);
		$this->add_control('css_class', [
			'label' => esc_html__('CSS class', 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
		]);
	}

	public function getBlockConfiguration(): array {
		// Getting back data
		$settings = $this->getSettingsForDisplay();
		$data['home_label'] = @$settings['home_label'] ?? $this->homeLabel;
		$data['blog_label'] = @$settings['blog_label'] ?? $this->blogLabel;
		$data['last_item_label'] = @$settings['last_item_label'] ?? $this->lastItemLabel;
		$data['last_item_has_link'] = @$settings['last_item_has_link'] ?? $this->lastItemHasLink;
		$data['generate_richsnippets'] = @$settings['generate_richsnippets'] ?? $this->generateRichsnippets;
		$data['separator'] = @$settings['separator'] ?? $this->separator;
		$data['css_class'] = @$settings['css_class'] ?? $this->cssClass;
		// Parsing data
		$data = array_map('trim', $data);
		$data['last_item_has_link'] = ($data['last_item_has_link'] === 'true') ? true : false;
		$data['generate_richsnippets'] = ($data['generate_richsnippets'] === 'true') ? true : false;
		// Returing data
		return $data;
	}

	protected function render() {
		$data = $this->getBlockConfiguration();
		$label = ($data['last_item_label'] !== '') ? $data['last_item_label'] : get_the_title();
		$html = '';
		$html .= '<div class="elementor-breadcrumb '.$data['css_class'].'">';
		$html .= '	<nav aria-label="breadcrumb">';
		$html .= '		<ul>';
		$html .= '			<li class="elementor-breadcrumb-home">';
		$html .= '				<a href="'.\Rou9e\Wordpress\Core::getHomeUrl().'">'.__($data['home_label'], 'elementor').'</a>';
		$html .= '			</li>';
		$html .= '			<li class="elementor-breadcrumb-separator">';
		$html .= '				'.$data['separator'];
		$html .= '			</li>';
		if(\Rou9e\Wordpress\Core::isPost()) {
			$html .= '			<li>';
			$html .= '				<a href="'.\Rou9e\Wordpress\Core::getBlogUrl().'">'.__($data['blog_label'], 'elementor').'</a>';
			$html .= '			</li>';
			$html .= '			<li class="elementor-breadcrumb-separator">';
			$html .= '				'.$data['separator'];
			$html .= '			</li>';
		}
		$html .= '			<li class="elementor-breadcrumb-last" aria-current="page">';
		if($data['last_item_has_link']) {
			$html .= '				<a href="'.get_permalink().'">';
		}
		$html .= '					'.$label;
		if($data['last_item_has_link']) {
			$html .= '				</a>';
		}
		$html .= '			</li>';
		$html .= '		</ul>';
		$html .= '	</nav>';
		if($data['generate_richsnippets']) {
			$html .= '	<script type="application/ld+json">';
			$html .= '		{';
			$html .= '			"@context": "https://schema.org",';
			$html .= '			"@type": "BreadcrumbList",';
			$html .= '			"itemListElement": [';
			$html .= '				{';
			$html .= '					"@type": "ListItem",';
			$html .= '					"position": 1,';
			$html .= '					"item": "'.\Rou9e\Wordpress\Core::getHomeUrl().'",';
			$html .= '					"name": "'.__($data['home_label'], 'elementor').'"';
			$html .= '				}';
			if(\Rou9e\Wordpress\Core::isPost()) {
				$html .= '				,{';
				$html .= '					"@type": "ListItem",';
				$html .= '					"position": 2,';
				$html .= '					"item": "'.\Rou9e\Wordpress\Core::getBlogUrl().'",';
				$html .= '					"name": "'.__($data['blog_label'], 'elementor').'"';
				$html .= '				}';
			}
			$html .= '				,{';
			$html .= '					"@type": "ListItem",';
			$html .= '					"position": '.((\Rou9e\Wordpress\Core::isPost()) ? 3 : 2).',';
			$html .= '					"item": "'.get_permalink().'",';
			$html .= '					"name": "'.$label.'"';
			$html .= '				}';
			$html .= '			]';
			$html .= '		}';
			$html .= '	</script>';
		}
		$html .= '</div>';
		echo $html;
	}
}

