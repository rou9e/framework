<?php

namespace Rou9e\Wordpress\Elementor;

if(!defined('ABSPATH')) { exit; }

abstract class AbstractElementorBlock extends \Elementor\Widget_Base {

	abstract public function getBlockName(): string;
	abstract public function getBlockTitle(): string;

	public bool $isCalledFromPhpTemplate = false;

	public function get_name() {
		return $this->getBlockName();
	}

	public function get_title() {
		return esc_html__($this->getBlockTitle(), 'elementor');
	}

	public function get_custom_help_url() {
		return 'https://developers.elementor.com/docs/widgets/';
	}

	public function get_categories() {
		return ['general'];
	}

	public function get_keywords() {
		return [];
	}

	public function get_icon() {
		// https://elementor.github.io/elementor-icons/
		return 'eicon-elementor-circle';
	}

	public function getSettingsForDisplay(): array {
		$settings = [];
		if($this->isCalledFromPhpTemplate) return $settings;
		try {
			try {
				$settings = $this->get_settings_for_display();
			}
			catch(\Error $e) {
				$settings = [];
			}
		}
		catch(\Exception $e) {
			$settings = [];
		}
		return $settings;
	}

	public function renderFromPhpTemplate() {
		$this->isCalledFromPhpTemplate = true;
		$this->render();
	}

	protected function register_controls() {
		// General
		$this->start_controls_section('general_section', [
			'label' => esc_html__('General', 'elementor'),
			'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
		]);
		$this->registerControlsSectionGeneral();
		// Settings
		$this->start_controls_section('settings_section', [
			'label' => esc_html__('Settings', 'elementor'),
			'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
		]);
		$this->registerControlsSectionSettings();
		$this->end_controls_section();
	}

	public function registerControlsSectionGeneral() {
		$this->add_control('title', [
			'label' => esc_html__('Title', 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
			'placeholder' => esc_html__('Some title', 'elementor'),
		]);
		$this->add_control('baseline', [
			'label' => esc_html__('Baseline', 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
			'placeholder' => esc_html__('Some baseline', 'elementor'),
		]);
		$this->add_control('image', [
			'label' => esc_html__('Image', 'elementor'),
			'type' => \Elementor\Controls_Manager::MEDIA,
			'default' => [
				'url' => '',
			],
		]);
	}

	public function registerControlsSectionSettings() {
		$this->add_control('css_class', [
			'label' => esc_html__('CSS class', 'elementor'),
			'type' => \Elementor\Controls_Manager::TEXT,
		]);
	}

	public function getBlockConfiguration(): array {
		// Getting back data
		$settings = $this->getSettingsForDisplay();
		$data['title'] = @$settings['title'] ?? '';
		$data['baseline'] = @$settings['baseline'] ?? '';
		$data['image'] = @$settings['image']['url'] ?? '';
		$data['css_class'] = @$settings['css_class'] ?? '';
		// Parsing data
		$data = array_map('trim', $data);
		// Returing data
		return $data;
	}

	protected function render() {
		$items = [];
		$data = $this->getBlockConfiguration();
		$html = '';
		$html .= '<div class="elementor-listing elementor-listing-'.$data['post_type'].' '.$data['css_class'].'">';
		if($data['title'] !== '') $html .= '<div class="elementor-listing-title">'.$data['title'].'</div>';
		if($data['baseline'] !== '') $html .= '<div class="elementor-listing-baseline">'.$data['baseline'].'</div>';
		if($data['image'] !== '') $html .= '<div class="elementor-listing-image"><img src="'.$data['image'].'" /></div>';
		$html .= '</div>';
		echo $html;
	}
}
