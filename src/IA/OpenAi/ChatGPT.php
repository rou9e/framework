<?php

namespace Rou9e\IA\OpenAi;

/*
	$prompt = "Comment utiliser des lunettes de piscine ?";
	$instructions = "Rédige deux réponses, une en langage soutenue, et une en langage familier. Chaque réponse doit faire autour de 100 mots.";
	$ia = new \Rou9e\IA\OpenAi\ChatGPT($_ENV['OPENAI_CHATGPT_KEY']);
	$response = $ia->getChatResponse($prompt, $instructions);
	dump($response);
	dump($ia->getChatTextualResponse());
*/

class ChatGPT {

	private string $_key = '';
	private string $_model = '';
	public ?\OpenAI\Client $client = null;
	public ?\OpenAI\Responses\Chat\CreateResponse $response = null;

	public function __construct(string $key = '', string $model = 'gpt-4o') {
		$this->_key = $key;
		$this->_model = $model;
		$this->client = \OpenAI::client($this->_key);
	}

	public function getChatParams(string $prompt = '', string $instructions = ''): array {
		if($prompt === '' && $instructions === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Params"]);
		$params = ['model' => $this->_model, 'messages' => []];
		if($prompt !== '') $params['messages'][] = ['role' => 'user', 'content' => $prompt];
		if($prompt !== '') $params['messages'][] = ['role' => 'system', 'content' => $instructions];
		return $params;
	}

	public function getChatResponse(string $prompt = '', string $instructions = ''): \OpenAI\Responses\Chat\CreateResponse {
		$params = $this->getChatParams($prompt, $instructions);
		$this->response = $this->client->chat()->create($params);
		return $this->response;
	}

	public function getChatTextualResponse(?\OpenAI\Responses\Chat\CreateResponse $response = null): string {
		if($response === null) $response = $this->response;
		if($response === null) return '';
		return $response->choices[0]->message->content;
	}
}
