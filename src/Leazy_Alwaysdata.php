<?php

/**
 * Leazy_Alwaysdata allows to request on Alwaysdata API
 *
 * @category		Leazy
 * @package			Leazy
 * @author			<leazy@nowayyy.com>
 *
 * https://help.alwaysdata.com/api/references
 */
class Leazy_Alwaysdata {

	// Attribut(s)															##################################################################################################
	public $_apiKey = '';												// ApiKey
	public $_options = array(										// Options
		CURLOPT_URL																=> '',
		CURLOPT_RETURNTRANSFER										=> true,
		CURLOPT_HEADER														=> false,
		CURLOPT_FOLLOWLOCATION										=> false,
		CURLOPT_HTTPAUTH													=> CURLAUTH_BASIC,
		CURLOPT_USERPWD														=> '',
		CURLOPT_FRESH_CONNECT											=> true,
	);

	// Constructor															##################################################################################################
	public function __construct($apiKey = '') {
		$this->setApiKey($apiKey);
	}

	// Get(s) & Set(s)														##################################################################################################
	// Allow to set ApiKey
	public function setApikey($apiKey = '') {
		if($apiKey == '') throw new Exception("'ApiKey' cannot be empty");
		$this->_apiKey = $apiKey;
		$this->_options[CURLOPT_USERPWD] = $this->_apiKey;
	}

	// Allow to set Account
	public function setAccount($account = '') {
		if($this->_apiKey == '') throw new Exception("Cannot set account before setting ApiKey");
		if($account == '') throw new Exception("'account' cannot be empty");
		$this->_options[CURLOPT_USERPWD] = $this->_apiKey.' account='.$account;
	}

	// Allow to set Url
	public function setUrl($url = '') {
		$this->_options[CURLOPT_URL] = $url;
	}

	// Allow to specify options
	public function addOptions($data = array()) {
		if(is_array($data)) foreach($data as $k => $v) $this->_options[$k] = $v;
	}

	// Allow to set POST parameters
	public function setData($data = array()) {
		$this->_options[CURLOPT_POSTFIELDS] = $data;
	}

	// Allow to set the type of request (GET / POST)
	public function setType($type = 'POST') {
		$this->_options[CURLOPT_HTTPGET] = false;
		$this->_options[CURLOPT_POST] = false;
		$this->_options[CURLOPT_CUSTOMREQUEST] = null;
		switch($type) {
			case 'GET':
				$this->_options[CURLOPT_HTTPGET] = true;
				break;
			case 'PATCH':
				$this->_options[CURLOPT_CUSTOMREQUEST] = 'PATCH';
				break;
			case 'DELETE':
				$this->_options[CURLOPT_CUSTOMREQUEST] = 'DELETE';
				break;
			case 'POST':
			default:
				$this->_options[CURLOPT_POST] = true;
				break;
		}
	}

	// Let's go !
	public function exec() {
		$curl = curl_init();
		curl_setopt_array($curl, $this->_options);
		$results = curl_exec($curl);
		if (FALSE === $results) throw new Exception(curl_error($curl), curl_errno($curl));
		curl_close($curl);
		return $results;
	}

	// Method(s)															##################################################################################################
	// Creating Alwaysdata account
	public function createAlwaysdataAccount($username, $password) {
		$this->setUrl('https://api.alwaysdata.com/v1/account/');
		$this->setType('POST');
		$this->setData(
			json_encode(
				array(
					'name' => $username,
					'password' => $password,
					'period' => '1y',
					'location_type' => 'server',
					'location_object' => '55',
					'product' => '8',
				)
			)
		);
		$response = $this->exec();
		//Leazy_Debug::dump($response);exit;
		if($response == '') return true;
		return $response;
	}

	// Deleting Alwaysdata account
	public function deleteAlwaysdataAccount($username) {

		// First we need the ID
		$this->setUrl('https://api.alwaysdata.com/v1/account/?name='.$username);
		$this->setType('GET');
		$response = $this->exec();
		$response = json_decode($response, true);
		//Leazy_Debug::dump($response);exit;
		if(!is_array($response) || empty($response)) return false;
		$response = array_shift($response);
		if(!isset($response['id']) || $response['id'] == '') return false;

		// Deleting account
		$this->setUrl('https://api.alwaysdata.com/v1/account/'.$response['id'].'/');
		$this->setType('DELETE');
		$response = $this->exec();
		//Leazy_Debug::dump($response);exit;
		if($response == '') return true;
		return $response;
	}

	// Creating FTP user
	public function createFtpUser($username, $password) {
		$this->setUrl('https://api.alwaysdata.com/v1/ftp/');
		$this->setType('POST');
		$this->setData(
			json_encode(
				array(
					'name' => $username,
					'password' => $password,
					'path' => '/',
				)
			)
		);
		$response = $this->exec();
		//Leazy_Debug::dump($response);exit;
		if($response == '') return true;
		return $response;
	}

	// Creating SSH user
	public function createSshUser($username, $password) {
		$this->setUrl('https://api.alwaysdata.com/v1/ssh/');
		$this->setType('POST');
		$this->setData(
			json_encode(
				array(
					'name' => $username,
					'password' => $password,
				)
			)
		);
		$response = $this->exec();
		//Leazy_Debug::dump($response);exit;
		if($response == '') return true;
		return $response;
	}

	// Enabling SSH user
	public function enableSshUser($username) {
		// First we need the ID
		$this->setUrl('https://api.alwaysdata.com/v1/ssh/?name='.$username);
		$this->setType('GET');
		$response = $this->exec();
		$response = json_decode($response, true);
		//Leazy_Debug::dump($response);exit;
		if(!is_array($response) || empty($response)) return false;
		$response = array_shift($response);
		if(!isset($response['id']) || $response['id'] == '') return false;

		// Deleting account
		$this->setUrl('https://api.alwaysdata.com/v1/ssh/'.$response['id'].'/');
		$this->setType('PATCH');
		$this->setData(
			json_encode(
				array(
					'is_enabled' => true,
				)
			)
		);
		$response = $this->exec();
		//Leazy_Debug::dump($response);exit;
		if($response == '') return true;
		return $response;
	}

	// Creating MySQL user
	public function createMysqlUser($username, $password) {
		$this->setUrl('https://api.alwaysdata.com/v1/database/user/');
		$this->setType('POST');
		$this->setData(
			json_encode(
				array(
					'name' => $username,
					'password' => $password,
					'type' => 'MYSQL'
				)
			)
		);
		$response = $this->exec();
		//Leazy_Debug::dump($response);exit;
		if($response == '') return true;
		return $response;
	}

	// Creating MySQL database
	public function createMysqlDatabase($database, $username) {
		$this->setUrl('https://api.alwaysdata.com/v1/database/');
		$this->setType('POST');
		$this->setData(
			json_encode(
				array(
					'name' => $database,
					'encoding' => 'utf8_unicode_ci',
					'type' => 'MYSQL',
					'permissions' => array(
						$username => 'FULL',
					),
				)
			)
		);
		$response = $this->exec();
		//Leazy_Debug::dump($response);exit;
		if($response == '') return true;
		return $response;
	}

	// Configuring PHP
	public function configurePHP($version = '5.6.16', $configuration = '') {
		$this->setUrl('https://api.alwaysdata.com/v1/environment/php/');
		$this->setType('PATCH');
		$this->setData(
			json_encode(
				array(
					'php5_version' => $version,
					'php5_ini_raw' => $configuration,
				)
			)
		);
		$response = $this->exec();
		//Leazy_Debug::dump($response);exit;
		if($response == '') return true;
		return $response;
	}

	// Add new website
	public function createNewWebsite($name = '', $urls = array(), $path = '') {
		$this->setUrl('https://api.alwaysdata.com/v1/site/');
		$this->setType('POST');
		$this->setData(
			json_encode(
				array(
					'name' => $name,
					'addresses' => $urls,
					'path' => $path,
					'type' => 'apache_standard',
				)
			)
		);
		$response = $this->exec();
		//Leazy_Debug::dump($response);exit;
		if($response == '') return true;
		return $response;
	}
}
