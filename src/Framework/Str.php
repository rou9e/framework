<?php

namespace Rou9e\Framework;

// Most of them come from https://laravel.com/docs/10.x/helpers#strings-method-list

class Str {

	/**
	 * Return the remainder of a string after the first occurrence of a given value.
	 *
	 * $slice = \Rou9e\Framework\Str::after('This is my name', 'This is');
	 * // ' my name'
	 *
	 */
	public static function after(string $subject = '', string $search = ''): string {
		return $search === '' ? $subject : array_reverse(explode($search, $subject, 2))[0];
	}

	/**
	 * Return the remainder of a string after the last occurrence of a given value.
	 *
	 * $slice = \Rou9e\Framework\Str::afterLast('App\Http\Controllers\Controller', '\\');
	 * // 'Controller'
	 *
	 */
	public static function afterLast(string $subject = '', string $search = ''): string {
		if($search === '') return $subject;
		$position = strrpos($subject, (string) $search);
		if($position === false) return $subject;
		return substr($subject, $position + strlen($search));
	}

	/**
	 * Get the portion of a string before the first occurrence of a given value.
	 *
	 * $slice = \Rou9e\Framework\Str::before('This is my name', 'my name');
	 * // 'This is '
	 *
	 */
	public static function before(string $subject = '', string $search = ''): string {
		if($search === '') return $subject;
		$result = strstr($subject, (string) $search, true);
		return $result === false ? $subject : $result;
	}

	/**
	 * Get the portion of a string before the last occurrence of a given value.
	 *
	 * $slice = \Rou9e\Framework\Str::beforeLast('This is my name', 'is');
	 * // 'This '
	 *
	 */
	public static function beforeLast(string $subject = '', string $search = ''): string {
		if($search === '') return $subject;
		$pos = mb_strrpos($subject, $search);
		if($pos === false) return $subject;
		return static::substr($subject, 0, $pos);
	}


	/**
	 * Get the portion of a string between two given values.
	 *
	 * $slice = \Rou9e\Framework\Str::between('This is my name', 'This', 'name');
	 * // ' is my '
	 *
	 */
	public static function between(string $subject = '', string $from = '', string $to = ''): string {
		if($from === '' || $to === '') return $subject;
		return static::beforeLast(static::after($subject, $from), $to);
	}

	/**
	 * Get the smallest possible portion of a string between two given values.
	 *
	 * $slice = \Rou9e\Framework\Str::betweenFirst('[a] bc [d]', '[', ']');
	 * // 'a'
	 *
	 */
	public static function betweenFirst(string $subject = '', string $from = '', string $to = ''): string {
		if($from === '' || $to === '') return $subject;
		return static::before(static::after($subject, $from), $to);
	}

	/**
	 * Convert a value to camel case.
	 *
	 * $converted = \Rou9e\Framework\Str::camel('foo_bar');
	 * // fooBar
	 *
	 */
	public static function camel(string $value = ''): string {
		return lcfirst(static::studly($value));
	}

	/**
	 * Determine if a given string contains a given substring.
	 *
	 * $contains = \Rou9e\Framework\Str::contains('This is my name', 'my');
	 * // true
	 *
	 */
	public static function contains(string $haystack = '', string $needle = '', bool $ignoreCase = false): bool {
		if($ignoreCase) $haystack = mb_strtolower($haystack);
		$needles = [$needle];
		foreach($needles as $needle) {
			if($ignoreCase) $needle = mb_strtolower($needle);
			if($needle !== '' && str_contains($haystack, $needle)) return true;
		}
		return false;
	}

	/*
	 * Determine if a given string ends with a given substring.
	 *
	 * $result = \Rou9e\Framework\Str::endsWith('This is my name', 'name');
	 * // true
	 *
	 */
	public static function endsWith(string $haystack = '', string $needle = ''): bool {
		$needles = [$needle];
		foreach($needles as $needle) {
			if((string) $needle !== '' && str_ends_with($haystack, $needle)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Cap a string with a single instance of a given value.
	 *
	 * $adjusted = \Rou9e\Framework\Str::finish('this/string', '/');
	 * // this/string/
	 *
	 * $adjusted = \Rou9e\Framework\Str::finish('this/string/', '/');
	 * // this/string/
	 *
	 */
	public static function finish(string $value = '', string $cap = ''): string {
		$quoted = preg_quote($cap, '/');
		return preg_replace('/(?:'.$quoted.')+$/u', '', $value).$cap;
	}

	/**
	 * Determine if a given string is valid JSON.
	 *
	 * $result = \Rou9e\Framework\Str::isJson('{"first": "John", "last": "Doe"}');
	 * // true
	 *
	 */
	public static function isJson(string $value = ''): bool {
		if(!is_string($value)) return false;
		try {
			json_decode($value, true, 512, JSON_THROW_ON_ERROR);
		}
		catch(\JsonException $e) {
			return false;
		}
		return true;
	}

	/**
	 * Convert a string to kebab case.
	 *
	 * $converted = \Rou9e\Framework\Str::kebab('fooBar');
	 * // foo-bar
	 *
	 */
	public static function kebab(string $value = ''): string {
		return static::snake($value, '-');
	}

	/**
	 * Make a string's first character lowercase.
	 *
	 * $string = \Rou9e\Framework\Str::lcfirst('Foo Bar');
	 * // foo Bar
	 *
	 */
	public static function lcfirst(string $string = ''): string {
		return static::lower(static::substr($string, 0, 1)).static::substr($string, 1);
	}

	/**
	 * Return the length of the given string.
	 *
	 * $length = \Rou9e\Framework\Str::length('Laravel');
	 * // 7
	 *
	 */
	public static function length(string $value = '', ?string $encoding = 'UTF-8') {
		return mb_strlen($value, $encoding);
	}

	/**
	 * Limit the number of characters in a string.
	 *
	 * $truncated = \Rou9e\Framework\Str::limit('The quick brown fox jumps over the lazy dog', 20);
	 * // The quick brown fox...
	 *
	 */
	public static function limit(string $value = '', int $limit = 100, string $end = '...') {
		if(mb_strwidth($value, 'UTF-8') <= $limit) return $value;
		return rtrim(mb_strimwidth($value, 0, $limit, '', 'UTF-8')).$end;
	}

	/**
	 * Convert the given string to lower-case.
	 *
	 * $converted = \Rou9e\Framework\Str::lower('LARAVEL');
	 * // laravel
	 *
	 */
	public static function lower(string $value = ''): string {
		return mb_strtolower($value, 'UTF-8');
	}

	/**
	 * Pad the left side of a string with another.
	 *
	 * $padded = \Rou9e\Framework\Str::padLeft('James', 10, '-');
	 * // '-----James'
	 *
	 */
	public static function padLeft(string $value = '', int $length = 0, string $pad = ' '): string {
		$short = max(0, $length - mb_strlen($value));
		return mb_substr(str_repeat($pad, $short), 0, $short).$value;
	}

	/**
	 * Pad the right side of a string with another.
	 *
	 * $padded = \Rou9e\Framework\Str::padRight('James', 10, '-');
	 * // 'James-----'
	 *
	 */
	public static function padRight(string $value = '', int $length = 0, string $pad = ' '): string {
		$short = max(0, $length - mb_strlen($value));
		return $value.mb_substr(str_repeat($pad, $short), 0, $short);
	}

	/**
	 * Generate a more truly "random" alpha-numeric string.
	 *
	 * $random = \Rou9e\Framework\Str::random(40);
	 *
	 */
	public static function random(int $length = 16): string {
		return (function ($length) {
			$string = '';
			while (($len = strlen($string)) < $length) {
				$size = $length - $len;
				$bytesSize = (int) ceil($size / 3) * 3;
				$bytes = random_bytes($bytesSize);
				$string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
			}
			return $string;
		})($length);
	}

	/**
	 * Remove any occurrence of the given string in the subject.
	 *
	 * $string = 'Peter Piper picked a peck of pickled peppers.';
	 * $removed = \Rou9e\Framework\Str::remove('e', $string);
	 * // Ptr Pipr pickd a pck of pickld ppprs.
	 *
	 */
	public static function remove(string $search = '', string $subject = '', $caseSensitive = true): string {
		return $caseSensitive ? (str_replace($search, '', $subject)) : (str_ireplace($search, '', $subject));
	}

	/**
	 * Reverse the given string.
	 *
	 * $reversed = \Rou9e\Framework\Str::reverse('Hello World');
	 * // dlroW olleH
	 *
	 */
	public static function reverse(string $value = ''): string {
		return implode(array_reverse(mb_str_split($value)));
	}

	/**
	 * Create a slug url based on the given string.
	 *
	 * $slug = \Rou9e\Framework\Str::slug('Laravel 5 Framework');
	 * // laravel-5-framework
	 *
	 */
	public static function slug(string $value = '', string $delimiter = '-'): string {
		$slug = iconv('UTF-8','ASCII//TRANSLIT', $value);
		$slug = strtr($slug, ['Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y']);
		$slug = str_replace(array('[\', \']'), '', $slug);
		$slug = preg_replace('/\[.*\]/U', '', $slug);
		$slug = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $slug);
		$slug = strip_tags($slug);
		$slug = preg_replace('/[\r\n\t ]+/', ' ', $slug);
		$slug = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $slug);
		$slug = html_entity_decode($slug, ENT_QUOTES, 'utf-8');
		$slug = htmlentities($slug, ENT_COMPAT, 'utf-8');
		$slug = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $slug );
		$slug = preg_replace(array('/[^0-9a-zA-Z\.\-]/i', '/[-]+/') , $delimiter, $slug);
		$slug = rawurlencode($slug);
		$slug = strtolower($slug);
		while(strpos($slug, $delimiter.$delimiter) !== FALSE) $slug = str_replace($delimiter.$delimiter, $delimiter, $slug);
		while(strpos($slug, $delimiter.'.') !== FALSE) $slug = str_replace($delimiter.'.', '.', $slug);
		while(strpos($slug, '..') !== FALSE) $slug = str_replace('..', '.', $slug);
		$slug = trim($slug, $delimiter);
		return $slug;
	}

	/**
	 * Convert a string to snake case.
	 *
	 * $converted = \Rou9e\Framework\Str::snake('fooBar');
	 * // foo_bar
	 *
	 * $converted = \Rou9e\Framework\Str::snake('fooBar', '-');
	 * // foo-bar
	 *
	 */
	public static function snake(string $value = '', string $delimiter = '_'): string {
		$key = $value;
		if(!ctype_lower($value)) {
			$value = preg_replace('/\s+/u', '', ucwords($value));
			$value = static::lower(preg_replace('/(.)(?=[A-Z])/u', '$1'.$delimiter, $value));
		}
		return $value;
	}

	/**
	 * Remove all "extra" blank space from the given string.
	 *
	 * $string = \Rou9e\Framework\Str::squish('    laravel    framework    ');
	 * // laravel framework
	 *
	 */
	public static function squish(string $value = ''): string {
		return preg_replace('~(\s|\x{3164})+~u', ' ', preg_replace('~^[\s\x{FEFF}]+|[\s\x{FEFF}]+$~u', '', $value));
	}

	/**
	 * Determine if a given string starts with a given substring.
	 *
	 * $result = \Rou9e\Framework\Str::startsWith('This is my name', 'This');
	 * // true
	 *
	 */
	public static function startsWith(string $haystack = '', string $needle = ''): bool {
		$needles = [$needle];
		foreach($needles as $needle) {
			if((string) $needle !== '' && str_starts_with($haystack, $needle)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Convert a value to studly caps case.
	 *
	 * $converted = \Rou9e\Framework\Str::studly('foo_bar');
	 * // FooBar
	 *
	 */
	public static function studly(string $value = ''): string {
		$key = $value;
		$words = explode(' ', str_replace(['-', '_'], ' ', $value));
		$studlyWords = array_map(fn ($word) => static::ucfirst($word), $words);
		return implode($studlyWords);
	}

	/**
	 * Returns the portion of the string specified by the start and length parameters.
	 *
	 * $converted = \Rou9e\Framework\Str::substr('The Laravel Framework', 4, 7);
	 * // Laravel
	 *
	 */
	public static function substr(string $string = '', int $start = 0, ?int $length = null, string $encoding = 'UTF-8'): string {
		return mb_substr($string, $start, $length, $encoding);
	}

	/**
	 * Returns the number of substring occurrences.
	 *
	 * $count = \Rou9e\Framework\Str::substrCount('If you like ice cream, you will like snow cones.', 'like');
	 * // 2
	 *
	 */
	public static function substrCount(string $haystack = '', string $needle = '', int $offset = 0, ?int $length = null): int {
		if(!is_null($length)) return substr_count($haystack, $needle, $offset, $length);
		return substr_count($haystack, $needle, $offset);
	}

	/**
	 * Convert the given string to title case.
	 *
	 * $converted = \Rou9e\Framework\Str::title('a nice title uses the correct case');
	 * // A Nice Title Uses The Correct Case
	 *
	 */
	public static function title(string $value = ''): string {
		return mb_convert_case($value, MB_CASE_TITLE, 'UTF-8');
	}

	/**
	 * Make a string's first character uppercase.
	 *
	 * $string = \Rou9e\Framework\Str::ucfirst('foo bar');
	 * // Foo bar
	 *
	 */
	public static function ucfirst(string $string = ''): string {
		return static::upper(static::substr($string, 0, 1)).static::substr($string, 1);
	}

	/**
	 * Split a string into pieces by uppercase characters.
	 *
	 * $segments = \Rou9e\Framework\Str::ucsplit('FooBar');
	 * // [0 => 'Foo', 1 => 'Bar']
	 *
	 */
	public static function ucsplit(string $string = ''): array {
		return preg_split('/(?=\p{Lu})/u', $string, -1, PREG_SPLIT_NO_EMPTY);
	}

	/**
	 * Convert the given string to upper-case.
	 *
	 * $string = \Rou9e\Framework\Str::upper('laravel');
	 * // LARAVEL
	 *
	 */
	public static function upper(string $value = ''): string {
		return mb_strtoupper($value, 'UTF-8');
	}
}
