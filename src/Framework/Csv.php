<?php

namespace Rou9e\Framework;

class Csv {

	// https://github.com/goodby/csv

	CONST MASK = '#@* %#';

	public string $delimiter = ';';
	public string $enclosure = '"';
	public string $encoding = 'UTF-8';
	public string $lineBreak = "\r\n";
	public string $escape = "\\";
	public bool $forceEnclosure = false;

	public function __construct(array $options = []) {
		if(isset($options['delimiter']) && is_string($options['delimiter']) && $options['delimiter'] !== '') $this->delimiter = $options['delimiter'];
		if(isset($options['enclosure']) && is_string($options['enclosure']) && $options['enclosure'] !== '') $this->enclosure = $options['enclosure'];
		if(isset($options['encoding']) && is_string($options['encoding']) && $options['encoding'] !== '') $this->encoding = $options['encoding'];
		if(isset($options['lineBreak']) && is_string($options['lineBreak']) && $options['lineBreak'] !== '') $this->lineBreak = $options['lineBreak'];
		if(isset($options['escape']) && is_string($options['escape']) && $options['escape'] !== '') $this->escape = $options['escape'];
		if(isset($options['forceEnclosure']) && is_bool($options['forceEnclosure']) && $options['forceEnclosure'] !== '') $this->forceEnclosure = $options['forceEnclosure'];
	}

	public function getTmpFilename() {
		$date = \Rou9e\Framework\Core::getFormattedDate('Ymd-Hisu');
		return '/var/tmp/'.$date.md5($date);
	}

	public function read(string $content = '', array $fields = [], bool $isFirstRowHeader = true): array {
		$data = [];
		if($content === '') return $data;
		// Configuration
		$config = new \Goodby\CSV\Import\Standard\LexerConfig();
		$config->setDelimiter($this->delimiter);
		$config->setEnclosure($this->enclosure);
		$config->setEscape($this->escape);
		$config->setToCharset($this->encoding);
		// Interpreter
		$interpreter = new \Goodby\CSV\Import\Standard\Interpreter();
		$interpreter->addObserver(function(array $row) use (& $data) { $data[] = $row; });
		// Creating a temporary file
		$filePath = $this->getTmpFilename();
		\Rou9e\Framework\Core::writeFile($filePath, $content);
		// Parser
		$importer = new \Goodby\CSV\Import\Standard\Lexer($config);
		$importer->parse($filePath, $interpreter);
		// Removing the temporary file
		\Rou9e\Framework\Core::writeFile($filePath);
		// Handling header
		if($isFirstRowHeader === true) {
			$rows = $data;
			$data = [];
			$header = array_shift($rows);
			foreach($rows as $row) $data[] = array_combine($header, $row);
			// Handling fields
			if(!empty($fields)) {
				$rows = $data;
				$data = [];
				foreach($rows as $row) $data[] = \Arr::only($row, $fields);
			}
		}
		return $data;
	}

	public function write(array $data = [], array $fields = [], bool $isFirstRowHeader = false): string {
		if($data === '') return '';
		// Configuration
		$config = new \Goodby\CSV\Export\Standard\ExporterConfig();
		$config->setDelimiter($this->delimiter);
		$config->setEnclosure($this->enclosure);
		$config->setEscape($this->escape);
		$config->setToCharset($this->encoding);
		$config->setNewline($this->lineBreak);
		// Handling fields
		if(!empty($fields)) {
			$rows = $data;
			$data = [];
			foreach($rows as $row) $data[] = \Arr::only($row, $fields);
		}
		// Handling header
		$header = ($isFirstRowHeader === true) ? array_shift($data) : array_keys($data[array_key_first($data)]);
		// Handling forceEnclosure
		if($this->forceEnclosure === true) {
			foreach($data as & $row) {
				foreach($row as & $column) {
					$column = static::MASK.$column.static::MASK;
				}
			}
			unset($row);unset($column);
			foreach($header as & $column) {
				$column = static::MASK.$column.static::MASK;
			}
			unset($column);
		}
		// Handling header
		$config->setColumnHeaders($header);
		// Creating a temporary file
		$filePath = $this->getTmpFilename();
		\Rou9e\Framework\Core::writeFile($filePath, '');
		// Parser
		$exporter = new \Goodby\CSV\Export\Standard\Exporter($config);
		$exporter->export($filePath, $data);
		$csv = \Rou9e\Framework\Core::readFile($filePath);
		// Handling forceEnclosure
		$csv = str_replace(static::MASK, '', $csv);
		// Removing the temporary file
		\Rou9e\Framework\Core::writeFile($filePath);
		return $csv;
	}
}
