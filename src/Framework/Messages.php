<?php

namespace Rou9e\Framework;

// \Rou9e\Framework\Messages::get('IS_MISSING', ['what' => "xxx"]);
// \Rou9e\Framework\Messages::throwException('IS_MISSING', ['what' => "xxx"]);

class Messages {

	// MISC
	const SPECIFIC_ERROR															= ":what";
	const RANDOM_ERROR																= "A random error occured";
	const SERVER_ERROR																= "A random error occured on the server";
	const ERROR_DETECTED															= "At least one error has been detected";
	const OPERATION_SUCCEDED													= "Operation succeded";
	const OPERATION_FAILED														= "Operation failed";
	const CONFIRMATION																= "Are you sure you want to continue ?";
	const NO_RESULT																		= "There is no result available";
	const MIN_X_ELEMENTS															= "A minimum of :count item(s) should be specified";
	const MAX_X_ELEMENTS															= "A maximum of :count item(s) should be specified";
	const EXACTLY_X_ELEMENTS													= "Exactly :count item(s) should be specified";

	// REQUESTS
	const NOT_GET																			= "Request should be a GET";
	const NOT_POST																		= "Request should be a POST";
	const NOT_AJAX																		= "Request should be AJAX";

	// PRIVILEGES / ROLES
	const NOT_ENOUGH_PRIVILEGES												= "You do not have enough privileges to perform this action";
	const LIMITED_PRIVILEGES													= "Your privileges are limited";

	// DATABASES
	const DATABASE_CONNECTION_SUCCESS									= "Database :what connection has succeded";
	const DATABASE_CONNECTION_FAILURE									= "Database :what connection has failed";

	// CLASSES
	const CLASS_ABSTRACT_INSTANCE											= "Abstract class `:class` can not be instantiated directly";
	const CLASS_ABSTRACT_METHOD												= "Abstract method should be implemented";

	// PARAM(S) | PROPERTY(S) | RECORD(S) | FILE(S) | DIRECTORY(S) | ...
	const IS_MISSING																	= ":what is missing";
	const IS_EMPTY																		= ":what can not be empty";
	const NOT_UNIQUE																	= ":what does already exist";
	const NOT_FOUND																		= ":what does not exist";
	const NOT_VALID																		= ":what is not valid";
	const NOT_STRING																	= ":what should be a string";
	const NOT_BOOLEAN																	= ":what should be a boolean";
	const NOT_INTEGER																	= ":what should be an integer";
	const NOT_ARRAY																		= ":what should be an array";
	const NOT_OBJECT																	= ":what should be an object";
	const NOT_URL																			= ":what should be an url";
	const NOT_CLASS																		= ":what should be an instance of `:class`";
	const NOT_CUSTOM																	= ":what should be :misc";
	const NOT_BELONG																	= ":what should belong to :misc";
	const NOT_ALLOWED																	= ":what should not belong to :misc";

	const OPEN_SUCCESS																= ":what has been opened";
	const READ_SUCCESS																= ":what has been read";
	const CREATE_SUCCESS															= ":what has been created";
	const UPDATE_SUCCESS															= ":what has been updated";
	const SAVE_SUCCESS																= ":what has been saved";
	const DELETE_SUCCESS															= ":what has been deleted";
	const EXPORT_SUCCESS															= ":what has been exported";
	const IMPORT_SUCCESS															= ":what has been imported";
	const MOVE_SUCCESS																= ":what has been moved";
	const DOWNLOAD_SUCCESS														= ":what has been downloaded";
	const UPLOAD_SUCCESS															= ":what has been uploaded";

	const OPEN_FAILURE																= ":what could not be opened";
	const READ_FAILURE																= ":what could not be read";
	const WRITE_FAILURE																= ":what could not be written";
	const CREATE_FAILURE															= ":what could not be created";
	const UPDATE_FAILURE															= ":what could not be updated";
	const SAVE_FAILURE																= ":what could not be saved";
	const DELETE_FAILURE															= ":what could not be deleted";
	const EXPORT_FAILURE															= ":what could not be exported";
	const IMPORT_FAILURE															= ":what could not be imported";
	const MOVE_FAILURE																= ":what could not be moved";
	const DOWNLOAD_FAILURE														= ":what could not be downloaded";
	const UPLOAD_FAILURE															= ":what could not be uploaded";

	const OPEN_MASSACTION															= "Total of :count :what has been opened";
	const READ_MASSACTION															= "Total of :count :what has been read";
	const CREATE_MASSACTION														= "Total of :count :what has been created";
	const UPDATE_MASSACTION														= "Total of :count :what has been updated";
	const SAVE_MASSACTION															= "Total of :count :what has been saved";
	const DELETE_MASSACTION														= "Total of :count :what has been deleted";
	const EXPORT_MASSACTION														= "Total of :count :what has been exported";
	const IMPORT_MASSACTION														= "Total of :count :what has been imported";
	const MOVE_MASSACTION															= "Total of :count :what has been moved";
	const DOWNLOAD_MASSACTION													= "Total of :count :what has been downloaded";
	const UPLOAD_MASSACTION														= "Total of :count :what has been uploaded";

	public static function get(string $code = '', $values = []): string {
		if($code === '') $code = 'RANDOM_ERROR';
		$code = '\Rou9e\Framework\Messages::'.$code;
		if(!defined($code)) throw new \Exception(\Rou9e\Framework\Messages::NOT_FOUND, ['what' => 'code']);
		$message = constant($code);
		foreach($values as $k => $v) {
			$message = str_replace(':'.$k, $v, $message);
		}
		return $message ;
	}

	public static function throwException(string $code = '', array $values = []): void {
		throw new \Exception(\Rou9e\Framework\Messages::get($code, $values), 1337);
	}
}
