<?php

namespace Rou9e\Framework;

// \Rou9e\Framework\Core::dump(xxx);

class Core {

	public static function dump($mixed, array $options = []) {
		// Validating input
		if(!isset($options['mode']) || !is_string($options['mode'])) $options['mode'] = 'var_dump';
		if(!isset($options['return']) || !is_bool($options['return'])) $options['return'] = false;
		$allowed = ['print_r', 'var_export', 'var_dump', 'text', 'json'];
		if(!in_array($options['mode'], $allowed)) \Rou9e\Framework\Messages::throwException('NOT_BELONG', ['what' => "Param options['mode']", 'misc' => ': '.implode(' | ', $allowed)]);
		// Disabling buffer rendering
		ob_start();
		// Displaying data
		if(!in_array($options['mode'], ['text'])) echo '<pre style="background-color: #F7F9C2;border: 1px solid rgba(0,0,0,0.15);font-family: monospace, sans-serif;font-size: 12px;line-height: 15px;border-radius: 0;display: block;margin: 0 0 10px;padding: 5px 10px;white-space: pre-wrap;word-break: break-all;word-wrap: break-word;text-align: left;color: #333333;">';
		if(!in_array($options['mode'], ['text'])) echo '<xmp>';
		if($options['mode'] === 'json') $mixed = json_encode($mixed, JSON_PRETTY_PRINT);
		switch($options['mode']) {
			case 'print_r':
				print_r($mixed);
			break;
			case 'var_export':
			case 'text':
			case 'json':
				var_export($mixed);
			break;
			case 'var_dump':
			default:
				var_dump($mixed);
			break;
		}
		if($options['mode'] !== 'text') echo '</xmp>';
		if($options['mode'] !== 'text') echo '</pre>';
		// Getting buffer rendering
		$output = ob_get_clean();
		if($options['return'] === true) return $output;
		echo $output;
	}

	public static function log($mixed = '', array $options = []) {
		// Validating input
		if(!isset($options['path']) || !is_string($options['path']) || $options['path'] === '') $options['path'] = '/var/tmp/rou9e-framework.log';
		if(!isset($options['prefix']) || !is_string($options['prefix'])) $options['prefix'] = '';
		if(!isset($options['date']) || !is_bool($options['date'])) $options['date'] = true;
		// Preparing data
		$content = $mixed;
		if(is_array($content) || is_object($content)) $content = \Rou9e\Framework\Core::dump($content, ['mode' => 'text', 'return' => true]);
		if($options['date']) $content = \Rou9e\Framework\Core::getFormattedDate().' // '.$content;
		$content = $options['prefix'].$content;
		$content = $content."\n";
		// Writing file
		\Rou9e\Framework\Core::writeFile($options['path'], $content, true);
	}

	public static function encaps($data = null): string {
		if($data == null) return '';
		return base64_encode(serialize($data));
	}

	public static function decaps(string $data = '') {
		if(!is_string($data) && $data == '') return [];
		return unserialize(base64_decode($data));
	}

	public static function jsonEncode($data = null): string {
		if($data == null) return '';
		return json_encode($data, JSON_UNESCAPED_UNICODE);
	}

	public static function jsonDecode(string $data = '', bool $throwException = false) {
		if($throwException === true) return json_decode($data, true, 512, JSON_THROW_ON_ERROR);
		return json_decode($data, true);
	}

	public static function getCurrentUrl() {
		return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?  "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}

	public static function parseUrl(?string $url = '', bool $self = false): array {
		$url = @$url ?? '';
		if(in_array($url, [null, ''])) $url = \Rou9e\Framework\Core::getCurrentUrl();
		$data = [
			'url'							=> '',
			'url_host'				=> '',
			'url_key_full'		=> '',
			'url_key_slug'		=> '',
			'url_key_path'		=> '',
			'url_key_dest'		=> '',
			'url_key_query'		=> '',
			'url_key_anchor'	=> '',
			'dirname'					=> '',
			'basename'				=> '',
			'path'						=> '',
			'scheme'					=> '',
			'host'						=> '',
			'query'						=> '',
		];
		if($self === true) {
			$data['method']		= '';
			$data['port']			= '';
			$data['get']			= [];
			$data['post']			= [];
			$data['request']	= [];
		}
		$data['url'] = $url;
		$data = array_merge($data, parse_url($data['url']));
		$data['path'] = trim($data['path'], '/');
		$data['dirname'] = trim($data['dirname'], '/');
		if($data['basename'] === '') $data['basename'] = basename($data['url']);
		if($data['dirname'] === '') $data['dirname'] = dirname($data['url']);
		$data['url_host'] = $data['scheme'].'://'.$data['host'].'/';
		if($data['url'] !== trim($data['url_host'], '/')) {
			$data['url_key_full'] = str_replace($data['url_host'], '', $data['url']);
			$tmp = explode('#', $data['url_key_full']);
			if(count($tmp) > 1) $data['url_key_anchor'] = '#'.array_pop($tmp);
			$data['url_key_query'] = str_replace(str_replace($data['path'], '', $data['url_key_full']), '', $data['url_key_query']);
			$data['url_key_slug'] = str_replace([$data['url_key_query'], $data['url_key_anchor']], '', $data['url_key_full']);
			$data['url_key_query'] = str_replace($data['path'], '', $data['url_key_slug']);
			$data['url_key_dest'] = basename($data['url_key_slug']);
			$tmp = explode('?', $data['url_key_dest']);
			$data['url_key_dest'] = $tmp[0];
			$data['url_key_path'] = str_replace([$data['url_key_dest'], $data['url_key_query']], '', $data['url_key_slug']);
		}
		if($self === true) {
			if($data['host'] === '') $data['host'] = $_SERVER['HTTP_HOST'];
			if($data['scheme'] === '') $data['scheme'] = $_SERVER['REQUEST_SCHEME'];
			if($data['port'] === '') $data['port'] = $_SERVER['SERVER_PORT'];
			if($data['method'] === '') $data['method'] = $_SERVER['REQUEST_METHOD'];
			if($data['query'] === '') $data['query'] = $_SERVER['QUERY_STRING'];
			if(empty($data['get'])) $data['get'] = $_GET;
			if(empty($data['post'])) $data['post'] = $_POST;
			if(empty($data['request'])) $data['request'] = $_REQUEST;
		}
		return $data;
	}

	public static function getCurrentPath() {
		return dirname(__FILE__).'/';
	}

	public static function getFormattedDate(string $format = 'Y-m-d H:i:s', int $timestamp = 0): string {
		if($timestamp === 0) $timestamp = time();
		return date($format, $timestamp);
	}

	public static function handleException(\Exception $e) {
		$value = $e;
		$value = [];
		$value['message'] = $e->getMessage();
		$value['error'] = $e->getFile()." >> LINE ".$e->getLine();
		$value['trace'] = "\n".$e->getTraceAsString();
		return $value;
	}

	public static function isSubstring(string $substring = '', string $string = ''): bool {
		return (strpos($string, $substring) === false) ? false : true;
	}

	public static function cleanList(string $content = '', array $separators = ['#', ',', ';', '█', "\t", "\n", "\r", "\0", "\x0B"], bool $unique = true): array {
		$content = trim($content);
		$content = str_replace($separators, ',', $content);
		$content = explode(',', $content);
		$content = array_filter($content);
		if($unique == true) $content = array_unique($content);
		$content = array_map('trim', $content);
		return $content;
	}

	public static function getInaccessibleClassProperty($object = null, string $property = '') {
		if($object === null) return false;
		if(is_object($object) === false) return false;
		if($property === '') return false;
		$reflectionClass = new \ReflectionClass($object);
		while($reflectionClass !== false && !$reflectionClass->hasProperty($property)) $reflectionClass = $reflectionClass->getParentClass();
		if($reflectionClass !== false) {
			$property = $reflectionClass->getProperty($property);
			$property->setAccessible(true);
			return $property;
			// This is how you get the property value :
			return $property->getValue($object);
		}
		return false;
	}

	public static function readFile(string $filename = ''): string {
		if($filename === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param 'filename'"]);
		if(!file_exists($filename)) \Rou9e\Framework\Messages::throwException('NOT_FOUND', ['what' => "File '".$filename."'"]);
		$content = file_get_contents($filename);
		if($content === false) \Rou9e\Framework\Messages::throwException('READ_FAILURE', ['what' => "File '".$filename."'"]);
		return $content;
	}

	public static function writeFile(string $filename = '', string $content = '', bool $append = false) {
		if($filename === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param 'filename'"]);
		$flag = ($append === true) ? FILE_APPEND : 0;
		$content = file_put_contents($filename, $content, $flag);
		if($content === false) {
			\Rou9e\Framework\Messages::throwException('WRITE_FAILURE', ['what' => "File '".$filename."'"]);
			return false;
		}
		return true;
	}

	public static function deleteFile(string $filename = ''): bool {
		if($filename === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param 'filename'"]);
		$content = unlink($filename);
		if($content === false) {
			\Rou9e\Framework\Messages::throwException('DELETE_FAILURE', ['what' => "File '".$filename."'"]);
			return false;
		}
		return true;
	}

	public static function getUrl(string $url = '', $options = []): string {
		if(@function_exists('curl_init')) {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Crawler version 1)');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
			foreach($options as $k => $v) {
				curl_setopt($curl, $k, $v);
			}
			$content = curl_exec($curl);
			curl_close($curl);
		} else {
			$content = file_get_contents($url);
		}
		return $content;
	}

	public static function sendEmailWithSmtp(string $subject = '', string $body = '', array $to = [], array $from = [], array $options = []) {
		// Validating input
		if($subject === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param 'subject'"]);
		if($body === '') \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param 'body'"]);
		if(empty($to)) \Rou9e\Framework\Messages::throwException('IS_EMPTY', ['what' => "Param 'to'"]);
		if(empty($from)) $from = ['hosting@rou9e.com' => 'Hosting rou9e'];
		if(!isset($options['microtime']) || !is_bool($options['microtime'])) $options['microtime'] = true;
		if(!isset($options['is_html']) || !is_bool($options['is_html'])) $options['is_html'] = true;
		if(!isset($options['host']) || !is_string($options['host']) || $options['host'] === '') $options['host'] = 'smtp.sendgrid.net';
		if(!isset($options['port']) || !is_int($options['port'])) $options['port'] = 587;
		if(!isset($options['timeout']) || !is_int($options['timeout'])) $options['timeout'] = 10;
		if(!isset($options['protocol']) || !is_string($options['protocol'])) $options['protocol'] = 'tls';
		if(!isset($options['charset']) || !is_string($options['charset']) || $options['charset'] === '') $options['charset'] = 'UTF-8';
		if(!isset($options['username']) || !is_string($options['username']) || $options['username'] === '') $options['username'] = 'apikey';
		if(!isset($options['password']) || !is_string($options['password']) || $options['password'] === '') $options['password'] = 'SG.mACERjh8Q...i7ddkcXXX';
		// Sending email
		$phpMailer = new \PHPMailer\PHPMailer\PHPMailer(true);
		$phpMailer->isHTML($options['is_html']);
		$phpMailer->isSMTP();
		$phpMailer->SMTPAuth = true;
		$phpMailer->CharSet = $options['charset'];
		$phpMailer->Timeout = $options['timeout'];
		$phpMailer->SMTPSecure = $options['protocol'];
		$phpMailer->Host = $options['host'];
		$phpMailer->Port = $options['port'];
		$phpMailer->Username = $options['username'];
		$phpMailer->Password = $options['password'];
		$phpMailer->Subject = (!$options['microtime']) ? $subject : $subject.' '.md5(microtime());
		$phpMailer->Body = $body;
		foreach($from as $email => $name) {
			$phpMailer->setFrom($email, $name);
			break;
		}
		foreach($to as $email => $name) {
			$phpMailer->addAddress($email, $name);
		}
		return ($phpMailer->send() == 1) ? true : false;
	}
}
