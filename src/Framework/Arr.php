<?php

namespace Rou9e\Framework;

// Most of them come from https://laravel.com/docs/10.x/helpers#arrays-and-objects-method-list

class Arr {

	/**
	 * Push an item onto the beginning of an array.
	 */
	public static function prepend(array & $array = [], $value): array {
		array_unshift($array, $value);
		return $array;
	}

	/**
	 * Push an item at the end of an array.
	 */
	public static function append(array & $array = [], $value): array {
		$array[] = $value;
		return $array;
	}

	/**
	 * Get the first element of an array.
	 */
	public static function first(array $array = []) {
		return reset($array);
	}

	/**
	 * Get the last element of an array.
	 */
	public static function last(array $array = []) {
		return end($array);
	}

	/**
	 * Determine if the given key exists in the provided array.
	 *
	 * $exists = \Rou9e\Framework\Arr::exists($array, 'name'); // true | false
	 *
	 */
	public static function exists(array $array = [], string $key = ''): bool {
		return array_key_exists($key, $array);
	}

	/**
	 * Shuffle the given array and return the result.
	 */
	public static function shuffle(array & $array = []): array {
		shuffle($array);
		return $array;
	}

	/**
	 * Get one or a specified number of random values from an array.
	 */
	public static function getRandom(array $array = []) {
		if(count($array) < 1) \Rou9e\Framework\Messages::throwException('NO_RESULT');
		return $array[array_rand($array)];
	}

	/**
	 * Set an array item to a given value using "dot" notation.
	 *
	 * $array = ['products' => ['desk' => ['price' => 100]]];
	 * \Rou9e\Framework\Arr::setDot($array, 'products.desk.price', 200);
	 *
	 */
	public static function setDot(array & $array = [], string $key = '', $value = null): array {
		if(is_null($key)) return $array = $value;
		$keys = explode('.', $key);
		foreach($keys as $i => $key) {
			if(count($keys) === 1) break;
			unset($keys[$i]);
			if(!isset($array[$key]) || !is_array($array[$key])) $array[$key] = [];
			$array = &$array[$key];
		}
		$array[array_shift($keys)] = $value;
		return $array;
	}

	/**
	 * Get an item from an array using "dot" notation.
	 *
	 * $array = ['products' => ['desk' => ['price' => 100]]];
	 * $price = \Rou9e\Framework\Arr::getDot($array, 'products.desk.price');
	 *
	 */
	public static function getDot(array $array = [], string $key = '', $default = null) {
		if(is_null($key)) return $array;
		if(static::exists($array, $key)) return $array[$key];
		if(strpos($key, '.') === false) return $array[$key] ?? $default;
		foreach(explode('.', $key) as $segment) {
			if(static::exists($array, $segment)) {
				$array = $array[$segment];
			}
			else {
				return $default;
			}
		}
		return $array;
	}

	/**
	 * Flatten a multi-dimensional associative array with dots.
	 *
	 * $array = ['products' => ['desk' => ['price' => 100]]];
	 * $flattened = \Rou9e\Framework\Arr::makeDot($array);
	 * // ['products.desk.price' => 100]
	 *
	 * @param  iterable  $array
	 * @param  string  $prepend
	 * @return array
	 */
	public static function makeDot(array $array = [], string $prepend = ''): array {
		$results = [];
		foreach($array as $key => $value) {
			if(is_array($value) && !empty($value)) {
				$results = array_merge($results, static::makeDot($value, $prepend.$key.'.'));
			}
			else {
				$results[$prepend.$key] = $value;
			}
		}
		return $results;
	}

	/**
	 * Convert a flatten "dot" notation array into an expanded array.
	 *
	 * $array = [
	 * 	'user.name' => 'Kevin Malone',
	 * 	'user.occupation' => 'Accountant',
	 * ];
	 * $array = \Rou9e\Framework\Arr::unmakeDot($array);
	 * // ['user' => ['name' => 'Kevin Malone', 'occupation' => 'Accountant']]
	 *
	 */
	public static function unmakeDot(array $array = []): array {
		$results = [];
		foreach($array as $key => $value) {
			static::setDot($results, $key, $value);
		}
		return $results;
	}

	/**
	 * Check if an item or items exist in an array using "dot" notation.
	 *
	 * $array = ['product' => ['name' => 'Desk', 'price' => 100]];
	 * $contains = \Rou9e\Framework\Arr::hasDot($array, 'product.name');
	 *
	 */
	public static function hasDot(array $array = [], string $key = ''): bool {
		$keys = (array) $key;
		if(!$array || $keys === []) return false;
		foreach($keys as $key) {
			$subKeyArray = $array;
			if(static::exists($array, $key)) {
				continue;
			}
			foreach(explode('.', $key) as $segment) {
				if(static::exists($subKeyArray, $segment)) {
					$subKeyArray = $subKeyArray[$segment];
				}
				else {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Remove one or many array items from a given array using "dot" notation.
	 *
	 * $array = ['products' => ['desk' => ['price' => 100], 'table' => ['price' => 50]]];
	 * Arr::forget($array, 'products.desk');
	 * // ['products' => ['table' => ['price' => 50]]]
	 *
	 */
	public static function forget(array & $array = [], string $key = '') {
		$original = & $array;
		if($key === '') return;
		if(static::exists($array, $key)) {
			unset($array[$key]);
			return;
		}
		$parts = explode('.', $key);
		// clean up before each pass
		$array = & $original;
		while(count($parts) > 1) {
			$part = array_shift($parts);
			if(isset($array[$part]) && is_array($array[$part])) {
				$array = &$array[$part];
			}
			else {
				return;
			}
		}
		unset($array[array_shift($parts)]);
	}
}
